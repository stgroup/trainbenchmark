# glpk also from https://github.com/dmccloskey/glpk/blob/master/Dockerfile

# Set the base image to Ubuntu
FROM ubuntu:latest
# Switch to root for install
USER root

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /home/user
RUN useradd --create-home --home-dir $HOME user \
    && chmod -R u+rwx $HOME \
    && chown -R user:user $HOME

RUN apt-get update -y && apt-get install -y \
	openjdk-8-jdk \
	r-base \
	r-base-dev \
	libudunits2-dev \
	cowsay \
	less \
	--no-install-recommends \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

COPY trainbenchmark/trainbenchmark-reporting/install.R /trainbenchmark/trainbenchmark-reporting/install.R
RUN Rscript /trainbenchmark/trainbenchmark-reporting/install.R

#USER user
WORKDIR /trainbenchmark

COPY trainbenchmark/ /trainbenchmark/
COPY docker /trainbenchmark/docker
COPY docker/bashrc /home/user/.bashrc
RUN chown -R user:user /trainbenchmark
RUN chown -R user:user /home/user/.bashrc
USER user

RUN ./gradlew --no-daemon preprocess \
	&& ./gradlew --no-daemon build shadowJar -x test \
	&& ./scripts/configure.sh 1 1 900 10 \
	&& ./gradlew --no-daemon generate \
	&& ./scripts/configure.sh 1 1024 900 10


# USER root
# RUN apt-get update -y && apt-get install -y \
# 	vim \
# 	--no-install-recommends \
# 	&& apt-get clean \
# 	&& rm -rf /var/lib/apt/lists/*
# USER user


# when benchmarking run:
# RUN ./gradlew --no-daemon generate
# RUN ./gradlew --no-daemon individualInjectBenchmark
# RUN ./gradlew --no-daemon individualRepairBenchmark
ENTRYPOINT /bin/bash
