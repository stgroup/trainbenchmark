#!/bin/bash
git clean -d -x -f
mkdir -p ModelValidationWithRAGs/trainbenchmark
mkdir -p ModelValidationWithRAGs/docker-results/
mkdir -p ModelValidationWithRAGs/docker-diagrams/

cp -a \
	.dockerignore \
	README.{md,html,txt} AUTHORS.txt \
	Dockerfile docker/ run_docker.sh \
	trainbenchmark/ \
	ModelValidationWithRAGs/ # the target directory

# remove build and gradle files
rm -rf ModelValidationWithRAGs/trainbenchmark/*/build
rm -rf ModelValidationWithRAGs/trainbenchmark/.gradle
# remove generate files
rm -rf ModelValidationWithRAGs/trainbenchmark/*/src/main/java-gen/*
rm -f ModelValidationWithRAGs/trainbenchmark/trainbenchmark-tool-jastadd-specialized-base/src/main/jastadd/TrainGen.*
rm ModelValidationWithRAGs/preprocessor/.gitignore

echo "Now please run '( cd ModelValidationWithRAGs/ && docker build -t trainbenchmark . ) && docker save --output ModelValidationWithRAGs/trainbenchmark-docker.tar trainbenchmark && chown $(whoami):$(whoami) ModelValidationWithRAGs/trainbenchmark-docker.tar'"
