function make() {
	echo "Make $1"
	unlink diagrams/recent
	unlink results/recent
	( cd diagrams && ln -s $1 recent )
	( cd results && ln -s $1 recent )
	./gradlew plotIndividual
}
make inject
make repair
target=~/git/v-papers/runtime-rags-slides/images/slides-
# target=~/git/v-papers/runtime-rags-slides/images/
for workload_directory in repair inject
do
	for phase in Read-and-Check Transformation-and-Recheck
	do
		for query in ConnectedSegments RouteSensor
		do
			cp diagrams/$workload_directory/$phase-$query-embed.pdf $target$workload_directory-$phase-$query.pdf;
		done
	done
done
# cp diagrams/repair/Read-and-Check-ConnectedSegments-embed.pdf $target/images/Read-and-Check-ConnectedSegments.pdf
# cp diagrams/repair/Read-and-Check-RouteSensor-embed.pdf $target/images/Read-and-Check-RouteSensor.pdf
# cp diagrams/inject/Transformation-and-Recheck-ConnectedSegments-embed.pdf $target/images/inject-Transformation-and-Recheck-ConnectedSegments.pdf
# cp diagrams/inject/Transformation-and-Recheck-RouteSensor-embed.pdf $target/images/inject-Transformation-and-Recheck-RouteSensor.pdf
# cp diagrams/repair/Transformation-and-Recheck-ConnectedSegments-embed.pdf $target/images/repair-Transformation-and-Recheck-ConnectedSegments.pdf
# cp diagrams/repair/Transformation-and-Recheck-RouteSensor-embed.pdf $target/images/repair-Transformation-and-Recheck-RouteSensor.pdf
