#!/usr/bin/env python
import argparse
import glob
import json
import logging
import os
import os.path
import re
import sys


FORMAT = '%(asctime)s %(levelname)-8s %(threadName)-10s (%(filename)s:%(lineno)d): %(message)s'
BENCHMARK_PATTERN = re.compile('.*-(BatchModel|Repair|Inject).*')
logger = logging.getLogger('merge_result')
SIZE_PATTERN = re.compile('.*-railway-[^\\-]*-([^\\-]*)-.csv')
NAME_PATTERN = re.compile('(times|matches)-([^\\-]*)-.*.csv')
RUN_PATTERN = re.compile('run-(....)-(..)-(..)-(..)-(..)-(..)')
RUN_REPLACMENT = r'\1_\2_\3 \4:\5:\6'


def include_file_config(args):
    def override_if_defined(key, convert=lambda x: x):
        keyArgs = key.replace('-', '_')
        value = content.get(key) or content.get(keyArgs)
        if value:
            setattr(args, keyArgs, convert(value))

    # load config file
    with open(args.file_config) as fdr:
        content = json.load(fdr)
    # update with local version, if existing
    directory, basename = os.path.split(os.path.abspath(args.file_config))
    local_config_file = os.path.join(directory, 'local-' + basename)
    if os.path.exists(local_config_file):
        with open(local_config_file) as fdr:
            content.update(json.load(fdr))
    else:
        logger.debug('No local config file found.')
    if not content.get('tools'):
        logger.error('Key "tools" not found in config file "' + args.file_config + '". Exiting.')
        sys.exit(1)
    args.tools = content['tools']
    override_if_defined('max-size', int)
    override_if_defined('dry-run', bool)
    override_if_defined('result-dir')
    override_if_defined('create-toolwise-dirs', bool)
    override_if_defined('no-clean', bool)
    override_if_defined('verbose', bool)


def create_link(fileToLink, linkName, dry_run):
    (logger.info if args.dry_run else logger.debug)('Linking %s to %s', fileToLink, linkName)
    if dry_run:
        return
    if os.path.lexists(linkName):
        os.unlink(linkName)
    os.symlink(fileToLink, linkName)


def ensure_directory(dir_name, dry_run):
    if dry_run:
        return
    if not os.path.exists(dir_name):
        logger.info('Creating %s', dir_name)
        os.mkdir(dir_name)


def exceeds(filename, max_size):
    match = SIZE_PATTERN.match(filename)
    return int(match.group(1)) > max_size if match else False


def remove_if_there(the_list, element_to_remove):
    if element_to_remove in the_list:
        the_list.remove(element_to_remove)


def new_run_name(old_run_name):
    return RUN_PATTERN.sub(RUN_REPLACMENT, old_run_name)


def copy_replace(fileTocopy, all_runs_dir, tool_name, run, dry_run):
    """
    Take fileTocopy, copy it to all_runs_dir, while replacing tool_name with run
    in both its name and its content
    """
    run_name = new_run_name(run)
    targetFile = os.path.join(
        all_runs_dir, os.path.basename(fileTocopy).replace(tool_name, run_name))
    if dry_run:
        logger.info('Would copy "%s" to "%s"', fileTocopy, targetFile)
        return
    logger.debug('Copying "%s" to "%s"', fileTocopy, targetFile)
    first = True
    with open(fileTocopy) as fdr_source, open(targetFile, 'w') as fdr_target:
        for line in fdr_source:
            if first:
                first = False
            else:
                line = line.replace(tool_name, run_name)
            fdr_target.write(line)


def main(args):
    """
    Main process.

    Used directory structure/variables:
    results/                        -> result_dir
        tools/                      -
            tool1/                  -> tool_dir
                tool1-run1/         -> run_dir
                    times.csv       -
                    matches.csv     -
                run-list.csv        -
                all-runs/           -> all_runs_dir
                    times-run1.csv@ -
        run1/                       -> global_run_dir
        merged/                     -> merged_dir
            individual/             -> merged_dir_individual
                inject/             -> merged_dir_individual_inject
                    times.csv@      - (only newest measurements of inject)
                repair/             -> merged_dir_individual_repair
                    times.csv@      - (only newest measurements of repair)
            benchmark/              -> merged_dir_benchmark
                times.csv@          - (only newest measurements of benchmark)
    """
    log_action = logger.info if args.dry_run else logger.debug
    unmatched_files = []

    # Gathering paths, creating top-level directories
    result_dir = os.path.abspath(args.result_dir)
    merged_dir = os.path.join(result_dir, 'merged')
    merged_dir_benchmark = os.path.join(merged_dir, 'benchmark')
    merged_dir_individual = os.path.join(merged_dir, 'individual')
    merged_dir_individual_inject = os.path.join(merged_dir_individual, 'inject')
    merged_dir_individual_repair = os.path.join(merged_dir_individual, 'repair')
    for dir_name in (merged_dir, merged_dir_benchmark, merged_dir_individual,
                     merged_dir_individual_inject, merged_dir_individual_repair):
        ensure_directory(dir_name, args.dry_run)

    # Using tools from config
    logger.debug('result_dir: %s, tools: %s', result_dir, args.tools)

    # Clean symlinks if requested or max_size is set
    if (args.clean or args.max_size) and not args.dry_run:
        for dir_to_clean in [merged_dir, merged_dir_benchmark, merged_dir_individual,
                             merged_dir_individual_inject, merged_dir_individual_repair]:
            for link in os.listdir(dir_to_clean):
                linkName = os.path.join(dir_to_clean, link)
                if os.path.islink(linkName):
                    os.unlink(linkName)
    if (args.clean or args.create_toolwise_dirs) and not args.dry_run:
        for linkName in glob.iglob(os.path.join(result_dir, 'tools', '*', 'all-runs', '*.csv')):
            os.remove(linkName)

    # Merge results
    for tool in args.tools:
        if tool.startswith('#'):
            logger.debug('Ignoring tool "%s"', tool[1:])
            continue
        already_merged = []
        tool_dir = os.path.join(result_dir, 'tools', tool)
        if not os.path.exists(tool_dir):
            logger.warn('Tool not found: %s', tool)
            continue
        all_runs_dir = os.path.join(tool_dir, 'all-runs')
        ensure_directory(all_runs_dir, args.dry_run)
        runs = sorted(os.listdir(tool_dir), reverse=True)
        remove_if_there(runs, 'all-runs')
        remove_if_there(runs, 'run-list.csv')

        if args.create_toolwise_dirs:
            # write out run-list.csv
            with open(os.path.join(tool_dir, 'run-list.csv'), 'w') as fdr:
                fdr.write('Runs\n')
                for run in runs:
                    fdr.write(new_run_name(run) + '\n')

        for run in runs:
            run_dir = os.path.join(tool_dir, run)
            global_run_dir = os.path.join(result_dir, run)
            if not os.path.isdir(run_dir):
                continue
            ensure_directory(global_run_dir, args.dry_run)
            for csvFile in os.listdir(run_dir):
                # link file in run directory
                fileToLink = os.path.join(run_dir, csvFile)
                linkName = os.path.join(global_run_dir, csvFile)
                create_link(fileToLink, linkName, args.dry_run)
                # skip if max-size is set and size is exceeded
                if args.max_size and exceeds(fileToLink, args.max_size):
                    continue
                if args.create_toolwise_dirs:
                    # link in all-runs (rename file accordingly)
                    match = NAME_PATTERN.match(csvFile)
                    if match:
                        if not BENCHMARK_PATTERN.match(csvFile):
                            tool_name = match.group(2)
                            copy_replace(fileToLink, all_runs_dir, tool_name, run, args.dry_run)
                    else:
                        unmatched_files.append(csvFile)
                # link file in merged directory
                if csvFile not in already_merged:
                    if BENCHMARK_PATTERN.match(csvFile):
                        target_dir = merged_dir_benchmark
                    elif 'inject' in csvFile:
                        target_dir = merged_dir_individual_inject
                    elif 'repair' in csvFile:
                        target_dir = merged_dir_individual_repair
                    else:
                        # this file has a strange name
                        logger.warn('Ignoring file "%s" with unknown pattern', fileToLink)
                    linkName = os.path.join(target_dir, csvFile)
                    create_link(fileToLink, linkName, args.dry_run)
                    already_merged.append(csvFile)
                else:
                    log_action('Skipping %s', csvFile)
    logging.info('%d files not matching pattern', len(unmatched_files))
    logging.debug('Files not matching pattern: %s', unmatched_files)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Merge results of all benchmark runs.',
        epilog="""The config file must contain the key "tools" specifying the tools to process.
        It further can contain any long version of arguments to this program as a default value.
        Any command line parameter will override such a default value.
        Additionally, a local version of the file will be read, overriding the default values.
        Its filename is "local-" prepended to the name of the config file.""")
    parser.add_argument(
        "-r", "--result-dir", help="Path to result directory to search in.", type=str)
    parser.add_argument(
        "-t", "--create-toolwise-dirs", help="Whether to recreate toolwise regression directories.",
        action="store_true")
    parser.add_argument(
        "-d", "--dry-run", help="Only print action, don't execute them.",
        action="store_true")
    parser.add_argument(
        "-n", "--no-clean", help="Don't remove previously existing symlinks in merged dir.",
        dest='clean', action="store_false", default=True)
    parser.add_argument(
        "-v", "--verbose", help="Print debug messages.", action="store_true")
    parser.add_argument(
        "-m", "--max-size", type=int,
        help="Maximum benchmark size to include. Implies cleaning existing symlinks.")
    parser.add_argument(
        "-f", "--file-config", default='merge_results.json', help="Config file to use.")
    args = parser.parse_args()
    include_file_config(args)
    logging.basicConfig(format=FORMAT, level=logging.DEBUG if args.verbose else logging.INFO)
    main(args)
