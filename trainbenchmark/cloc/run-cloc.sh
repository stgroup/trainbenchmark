#!/usr/bin/bash
shopt -s globstar

if [ ]
then
	:
else

#  --force-lang=Java,jrag --force-lang=Java,jadd
DEF_FILE=my_definitions.txt
echo "Export language definitions"
cloc --write-lang-def="$DEF_FILE" 2>cloc-errors.log
for f in cloc-def-*.txt;
do
	cat $f >> "$DEF_FILE"
done
echo "Running cloc with new definitions"
#  --ignored=bad-files.txt
CLOC_CMD="cloc --exclude-lang=JSON --read-lang-def=my_definitions.txt --exclude-list-file=.clocignore --quiet"
$CLOC_CMD --report-file=base-result.txt ../trainbenchmark-tool-jastadd-base/src/main/jastadd/{Helpers.*,Enums.jadd} 2>>cloc-errors.log

$CLOC_CMD --report-file=namelookup-result.txt ../trainbenchmark-tool-jastadd-namelookup-base/src/main/jastadd/*.j{add,rag} 2>>cloc-errors.log
$CLOC_CMD --report-file=intrinsic-result.txt ../trainbenchmark-tool-jastadd-intrinsic-base/src/main/jastadd/*.j{add,rag} 2>>cloc-errors.log
$CLOC_CMD --report-file=relrag-result.txt ../trainbenchmark-tool-jastadd-relast-base/src/main/jastadd/*.j{add,rag} 2>>cloc-errors.log

$CLOC_CMD --report-file=namelookup-queries-result.txt ../trainbenchmark-tool-jastadd-namelookup-base/src/main/jastadd/queries 2>>cloc-errors.log
$CLOC_CMD --report-file=intrinsic-queries-result.txt ../trainbenchmark-tool-jastadd-intrinsic-base/src/main/jastadd/queries 2>>cloc-errors.log
$CLOC_CMD --report-file=relrag-queries-result.txt ../trainbenchmark-tool-jastadd-relast-base/src/main/jastadd/queries 2>>cloc-errors.log

$CLOC_CMD --report-file=namelookup-transfo-result.txt ../trainbenchmark-tool-jastadd-namelookup/src/main/java-include/**/transformations/{inject,repair} 2>>cloc-errors.log
$CLOC_CMD --report-file=intrinsic-transfo-result.txt ../trainbenchmark-tool-jastadd-intrinsic/src/main/java-include/**/transformations/{inject,repair} 2>>cloc-errors.log
$CLOC_CMD --report-file=relrag-transfo-result.txt ../trainbenchmark-tool-jastadd-relast/src/main/java-include/**/transformations/{inject,repair} 2>>cloc-errors.log

$CLOC_CMD --report-file=ser-namelookup-result.txt ../trainbenchmark-tool-jastadd-namelookup/src/main/java-include/de/tudresden/inf/st/train/jastadd/driver/deserializer/ASTNodeDeserializer.java 2>>cloc-errors.log
$CLOC_CMD --report-file=ser-intrinsic-result.txt ../trainbenchmark-tool-jastadd-intrinsic/src/main/java-include/de/tudresden/inf/st/train/jastadd/driver/deserializer/ASTNodeDeserializer.java 2>>cloc-errors.log
$CLOC_CMD --report-file=ser-relrag-manual-result.txt ../trainbenchmark-tool-jastadd-manual-relast/src/main/java/de/tudresden/inf/st/train/jastadd/driver/deserializer/ASTNodeDeserializer.java 2>>cloc-errors.log
# $CLOC_CMD --report-file=ser-relrag-generated-result.txt ../trainbenchmark-tool-jastadd-relast/src/main/java 2>>cloc-errors.log
# $CLOC_CMD --sum-reports --report_file=trainbenchmark namelookup-result.txt intrinsic-result.txt relrag-result.txt relrag-manual-result.txt relrag-generated-result.txt 2>>cloc-errors.log

CFC_CMD='grep -o 'if'\|'for'\|'return''
echo "CFC stats" > cfcs.txt
echo "base: $($CFC_CMD ../trainbenchmark-tool-jastadd-base/src/main/jastadd/{Helpers.*,Enums.jadd} | wc -l)" >> cfcs.txt
echo "namelookup: $($CFC_CMD ../trainbenchmark-tool-jastadd-namelookup-base/src/main/jastadd/*.j{add,rag} | wc -l) + queries: $($CFC_CMD ../trainbenchmark-tool-jastadd-namelookup-base/src/main/jastadd/queries/* | wc -l)" >> cfcs.txt
echo "intrinsic: $($CFC_CMD ../trainbenchmark-tool-jastadd-intrinsic-base/src/main/jastadd/*.j{add,rag} | wc -l) + queries: $($CFC_CMD ../trainbenchmark-tool-jastadd-intrinsic-base/src/main/jastadd/queries/* | wc -l)" >> cfcs.txt
echo "relast: $($CFC_CMD ../trainbenchmark-tool-jastadd-relast-base/src/main/jastadd/*.j{add,rag} | wc -l) + queries: $($CFC_CMD ../trainbenchmark-tool-jastadd-relast-base/src/main/jastadd/queries/* | wc -l)" >> cfcs.txt
cat cfcs.txt

AC_CMD='grep -w 'syn'\|'inh''
# \|inh
echo "AC stats" > acs.txt
echo "base: $($AC_CMD ../trainbenchmark-tool-jastadd-base/src/main/jastadd/{Helpers.*,Enums.jadd} | wc -l)" >> acs.txt
echo "namelookup: $($AC_CMD ../trainbenchmark-tool-jastadd-namelookup-base/src/main/jastadd/*.j{add,rag} | wc -l) + queries: $($AC_CMD ../trainbenchmark-tool-jastadd-namelookup-base/src/main/jastadd/queries/* | wc -l)" >> acs.txt
echo "intrinsic: $($AC_CMD ../trainbenchmark-tool-jastadd-intrinsic-base/src/main/jastadd/*.j{add,rag} | wc -l) + queries: $($AC_CMD ../trainbenchmark-tool-jastadd-intrinsic-base/src/main/jastadd/queries/* | wc -l)" >> acs.txt
echo "relast: $($AC_CMD ../trainbenchmark-tool-jastadd-relast-base/src/main/jastadd/*.j{add,rag} | wc -l) + queries: $($AC_CMD ../trainbenchmark-tool-jastadd-relast-base/src/main/jastadd/queries/* | wc -l)" >> acs.txt
cat acs.txt

fi

echo "LOC stats"
echo "Language                     files          blank        comment           code"
( for t in *-result.txt ; do echo -e "==> $t <=="; grep -v -e '---' -e 'SUM' -e 'Language' -e 'github' $t; done)
