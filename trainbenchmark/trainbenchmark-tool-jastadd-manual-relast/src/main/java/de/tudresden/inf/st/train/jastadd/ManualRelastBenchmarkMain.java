package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.ManualRelastBenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;

public class ManualRelastBenchmarkMain {
  public static void main(final String[] args) throws Exception {
    final ManualRelastBenchmarkConfig bc = BenchmarkConfig.fromFile(args[0], ManualRelastBenchmarkConfig.class);
    final ManualRelastBenchmarkScenario scenario = new ManualRelastBenchmarkScenario(bc);
    scenario.performBenchmark();
    scenario.printTraces();
  }
}
