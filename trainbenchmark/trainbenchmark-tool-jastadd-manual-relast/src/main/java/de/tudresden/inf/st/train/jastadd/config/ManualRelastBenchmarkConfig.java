package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;

public class ManualRelastBenchmarkConfig extends JastaddAbstractBenchmarkConfig {

  protected ManualRelastBenchmarkConfig(final BenchmarkConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getToolName() {
    return "Grammar Extension with Manual Serialization";
  }

  @Override
  public String getProjectName() {
    return "jastadd-manual-relast";
  }

}
