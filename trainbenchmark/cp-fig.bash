target=~/work/git/RuntimeRAGs/fig/
# target=~/git/v-papers/runtime-rags-slides/images/slides-
# target=~/git/v-papers/runtime-rags-slides/images/
phases="Read-and-Check Transformation-and-Recheck"
queries="ConnectedSegments RouteSensor SwitchSet"
for workload_directory in repair inject
do
    for phase in $phases
    do
        for query in $queries
        do
            cp diagrams/recent/$workload_directory/$phase-$query-$workload_directory.pdf $target$phase-$query-$workload_directory.pdf;
        done
    done
done

for workload_directory in repair inject
do
    for phase in $phases
    do
        cp diagrams/recent/benchmark/$workload_directory/$phase-$workload_directory.pdf $target$phase-$workload_directory.pdf;
    done
done

cp diagrams/recent/benchmark/inject/Read-inject.pdf ${target}Read-inject.pdf
