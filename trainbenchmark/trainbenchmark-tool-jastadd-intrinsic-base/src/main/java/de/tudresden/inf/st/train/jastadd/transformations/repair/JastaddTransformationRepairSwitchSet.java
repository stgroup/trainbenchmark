package de.tudresden.inf.st.train.jastadd.transformations.repair;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchSetMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationRepairSwitchSet<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddSwitchSetMatch, TJastaddDriver> {

  public JastaddTransformationRepairSwitchSet(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddSwitchSetMatch> matches) {
    for (final JastaddSwitchSetMatch match : matches) {
      match.getSw().setCurrentPosition(match.getSwP().getPosition());
    }

    driver.flushCache();
  }

}
