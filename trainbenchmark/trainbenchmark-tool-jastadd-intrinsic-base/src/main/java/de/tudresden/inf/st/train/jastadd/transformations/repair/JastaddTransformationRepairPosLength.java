package de.tudresden.inf.st.train.jastadd.transformations.repair;

import de.tudresden.inf.st.train.jastadd.ast.Segment;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddPosLengthMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationRepairPosLength<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddPosLengthMatch, TJastaddDriver> {

  public JastaddTransformationRepairPosLength(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddPosLengthMatch> matches) {
    for (final JastaddPosLengthMatch plm : matches) {
      final Segment segment = plm.getSegment();
      segment.setLength(-segment.getLength() + 1);
    }
    driver.flushCache();
  }

}
