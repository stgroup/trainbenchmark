package de.tudresden.inf.st.train.jastadd.transformations.repair;


import de.tudresden.inf.st.train.jastadd.ast.*;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddConnectedSegmentsMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationRepairConnectedSegments<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddConnectedSegmentsMatch, TJastaddDriver> {

  public JastaddTransformationRepairConnectedSegments(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddConnectedSegmentsMatch> matches) {
    for (final JastaddConnectedSegmentsMatch match : matches) {

      final Segment segment2 = match.getSegment2();

      // delete all references to segment2 (monitoring sensors and other segments connecting to it)
      for (Region region : driver.getModel().getRegionList()) {
        for (Sensor sensor : region.getSensorList()) {
          for (int i = sensor.getMonitoredElementList().getNumChild() - 1; i >= 0; i--) {
            if (sensor.getMonitoredElement(i).getValue() == segment2.id()) {
              sensor.getMonitoredElement(i).removeSelf();
            }
          }
        }
        for (TrackElement element : region.getTrackElementList()) {
          for (int i = element.getConnectedElementList().getNumChild() - 1; i >= 0; i--) {
            if (element.getConnectedElement(i).getValue() == segment2.id()) {
              element.getConnectedElement(i).removeSelf();
            }
          }
        }
      }

      segment2.removeSelf();

      // connect segment1 to segment3
      match.getSegment1().addConnectedElement(match.getSegment3().createRef());
    }
    driver.flushCache();
  }

}
