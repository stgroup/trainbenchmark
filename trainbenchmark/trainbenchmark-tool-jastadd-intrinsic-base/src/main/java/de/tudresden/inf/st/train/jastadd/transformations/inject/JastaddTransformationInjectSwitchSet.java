package de.tudresden.inf.st.train.jastadd.transformations.inject;


import de.tudresden.inf.st.train.jastadd.ast.Position;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchSetInjectMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationInjectSwitchSet<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddSwitchSetInjectMatch, TJastaddDriver> {

  public JastaddTransformationInjectSwitchSet(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddSwitchSetInjectMatch> matches) {
    for (final JastaddSwitchSetInjectMatch match : matches) {
      Position oldPosition = match.getSw().getCurrentPosition();
      Position newPosition = oldPosition == Position.DIVERGING ? Position.FAILURE : (oldPosition == Position.FAILURE ? Position.STRAIGHT : Position.DIVERGING);
      match.getSw().setCurrentPosition(newPosition);
    }
    driver.flushCache();
  }

}
