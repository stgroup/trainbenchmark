package de.tudresden.inf.st.train.jastadd.transformations.inject;

import de.tudresden.inf.st.train.jastadd.ast.*;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddConnectedSegmentsInjectMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;
import hu.bme.mit.trainbenchmark.constants.TrainBenchmarkConstants;

import java.util.Collection;

public class JastaddTransformationInjectConnectedSegments<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddConnectedSegmentsInjectMatch, TJastaddDriver> {

  public JastaddTransformationInjectConnectedSegments(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddConnectedSegmentsInjectMatch> matches) {
    for (final JastaddConnectedSegmentsInjectMatch match : matches) {
      // create segment2
      List<TrackElementRef> connectedSegments = new List<>();
      connectedSegments.add(match.getSegment3().createRef());
      List<Semaphore> semaphores = new List<>();
      Segment segment2 = new Segment(driver.nextId(), connectedSegments, TrainBenchmarkConstants.DEFAULT_SEGMENT_LENGTH, semaphores);

      match.getSegment1().containingRegion().addTrackElement(segment2);

      // have the sensor monitor the segment
      match.getSensor().addMonitoredElement(segment2.createRef());

      // remove the connection of segment1 to segment3
      for (int i = 0; i < match.getSegment1().getConnectedElementList().getNumChild(); i++) {
        if (match.getSegment1().getConnectedElement(i).getValue() == match.getSegment3().id()) {
          match.getSegment1().getConnectedElementList().removeChild(i);
          break;
        }
      }

      // connect segment1 to segment2
      match.getSegment1().addConnectedElement(segment2.createRef());
    }
    driver.flushCache();
  }

}
