aspect Queries {

  syn Collection<JastaddConnectedSegmentsMatch> RailwayContainer.connectedSegmentsMatches() {
    java.util.List<JastaddConnectedSegmentsMatch> matches = new java.util.ArrayList<>();
    for (Region region: getRegionList()) {
      matches.addAll(region.connectedSegmentsMatches());
    }
    return matches;
  }

  syn Collection<JastaddConnectedSegmentsMatch> Region.connectedSegmentsMatches() {
    java.util.List<JastaddConnectedSegmentsMatch> matches = new java.util.ArrayList<>();
    for (Sensor sensor: getSensorList()) {
      matches.addAll(sensor.connectedSegmentsMatches());
    }
    return matches;
  }

  syn Collection<JastaddConnectedSegmentsMatch> Sensor.connectedSegmentsMatches() {
  java.util.List<JastaddConnectedSegmentsMatch> matches = new java.util.ArrayList<>();
    for (Segment segment: monitoredSegments()) {
      sequenceLoop: for (java.util.List<Segment> segmentSequence : segment.transConnectedSegments()) {
        if (segmentSequence.size() < 5) continue sequenceLoop;
        for (int index = 0; index < 5; index++) {
          if (!this.monitors(segmentSequence.get(index))) continue sequenceLoop;
        }
        matches.add(new JastaddConnectedSegmentsMatch(this, segment, segmentSequence.get(0), segmentSequence.get(1), segmentSequence.get(2), segmentSequence.get(3), segmentSequence.get(4)));
      }
    }
    return matches;
  }

  syn java.util.SortedSet<java.util.List<Segment>> Segment.transConnectedSegments() {
    java.util.TreeSet<java.util.List<Segment>> results = new java.util.TreeSet<java.util.List<Segment>>(new ElementComparator(5));
    for (Segment segment : this.connectsToSegments()) {
      Collection<java.util.List<Segment>> segmentResult = segment.transConnectedSegments();
      if (segmentResult.isEmpty()) {
        java.util.List<Segment> newList = new java.util.ArrayList<Segment>();
        newList.add(segment);
        results.add(newList);
      } else {
        for (java.util.List<Segment> segmentList: segmentResult) {
          java.util.List<Segment> newList = new java.util.ArrayList<Segment>();
          newList.add(segment);
          for (int index = 0; index < Math.min(segmentList.size(),5); index++) {
            newList.add(segmentList.get(index));
          }
          results.add(newList);
        }
      }
    }
    return results;
  }

  syn Collection<Segment> TrackElement.connectedSegments(Sensor sensor) {
    java.util.List<Segment> segments = new java.util.ArrayList<>();
    for (Segment segment : this.connectsToSegments()) {
      if (sensor.monitors(segment)){
        segments.add(segment);
      }
    }
    return segments;
  }

  syn boolean Sensor.monitors(Segment segment) {
    for (Ref ref : getMonitoredElements()) {
      if (ref.references(segment)) {
        return true;
      }
    }
    return false;
  }

  syn Collection<JastaddConnectedSegmentsInjectMatch> RailwayContainer.connectedSegmentsInjectMatches() {
    java.util.List<JastaddConnectedSegmentsInjectMatch> matches = new java.util.ArrayList<>();
    for (Region region : getRegionList()) {
      matches.addAll(region.connectedSegmentsInjectMatches());
    }
    return matches;
  }

  syn Collection<JastaddConnectedSegmentsInjectMatch> Region.connectedSegmentsInjectMatches() {
    java.util.List<JastaddConnectedSegmentsInjectMatch> matches = new java.util.ArrayList<>();
    for (Sensor sensor : getSensorList()) {
      matches.addAll(sensor.connectedSegmentsInjectMatches());
    }
    return matches;
  }

  syn Collection<JastaddConnectedSegmentsInjectMatch> Sensor.connectedSegmentsInjectMatches() {
    java.util.List<JastaddConnectedSegmentsInjectMatch> matches = new java.util.ArrayList<>();
    for (Segment segment1: monitoredSegments()) {
      for(Segment segment3:segment1.connectedSegments(this)){
        matches.add(new JastaddConnectedSegmentsInjectMatch(this,segment1,segment3));
      }
    }
    return matches;
  }

}
