package de.tudresden.inf.st.train.jastadd.test;

import de.tudresden.inf.st.train.jastadd.RelASTBenchmarkScenario;
import de.tudresden.inf.st.train.jastadd.config.RelASTBenchmarkConfig;
import de.tudresden.inf.st.train.jastadd.config.RelASTBenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;
import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkResult;
import hu.bme.mit.trainbenchmark.benchmark.test.TrainBenchmarkTest;

public class RelASTSerializerTest extends TrainBenchmarkTest {

  @Override
  protected BenchmarkResult runTest(final BenchmarkConfigBase bcb) throws Exception {
    final RelASTBenchmarkConfig bc = new RelASTBenchmarkConfigBuilder().setConfigBase(bcb).createConfig();
    final RelASTBenchmarkScenario scenario = new RelASTBenchmarkScenario(bc);
    final BenchmarkResult result = scenario.performBenchmark();
    return result;
  }

}
