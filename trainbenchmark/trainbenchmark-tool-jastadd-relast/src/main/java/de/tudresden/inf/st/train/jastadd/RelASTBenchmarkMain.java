package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.RelASTBenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;

public class RelASTBenchmarkMain {
  public static void main(final String[] args) throws Exception {
    final RelASTBenchmarkConfig bc = BenchmarkConfig.fromFile(args[0], RelASTBenchmarkConfig.class);
    final RelASTBenchmarkScenario scenario = new RelASTBenchmarkScenario(bc);
    scenario.performBenchmark();
    scenario.printTraces();
  }
}
