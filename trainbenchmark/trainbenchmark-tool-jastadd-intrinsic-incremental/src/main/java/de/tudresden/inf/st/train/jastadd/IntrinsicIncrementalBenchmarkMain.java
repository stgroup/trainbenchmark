package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.IntrinsicIncrementalBenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;

public class IntrinsicIncrementalBenchmarkMain {
  public static void main(final String[] args) throws Exception {
    final IntrinsicIncrementalBenchmarkConfig bc = BenchmarkConfig.fromFile(args[0], IntrinsicIncrementalBenchmarkConfig.class);
    final IntrinsicIncrementalBenchmarkScenario scenario = new IntrinsicIncrementalBenchmarkScenario(bc);
    scenario.performBenchmark();
    scenario.printTraces();
  }
}
