package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;

public class IntrinsicIncrementalBenchmarkConfig extends JastaddAbstractBenchmarkConfig {

  protected IntrinsicIncrementalBenchmarkConfig(final BenchmarkConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getToolName() {
    return "Intrinsic References (Incremental)";
  }

  @Override
  public String getProjectName() {
    return "jastadd-intrinsic-incremental";
  }

}
