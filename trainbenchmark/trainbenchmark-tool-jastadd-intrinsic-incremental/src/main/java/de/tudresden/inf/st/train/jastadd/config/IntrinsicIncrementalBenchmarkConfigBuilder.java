package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBuilder;

public class IntrinsicIncrementalBenchmarkConfigBuilder extends BenchmarkConfigBuilder<IntrinsicIncrementalBenchmarkConfig, IntrinsicIncrementalBenchmarkConfigBuilder> {

  @Override
  public IntrinsicIncrementalBenchmarkConfig createConfig() {
    checkNotNulls();
    return new IntrinsicIncrementalBenchmarkConfig(configBase);
  }

}
