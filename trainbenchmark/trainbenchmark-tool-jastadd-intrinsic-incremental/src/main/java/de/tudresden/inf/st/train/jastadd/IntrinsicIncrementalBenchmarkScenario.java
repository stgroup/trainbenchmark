package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.IntrinsicIncrementalBenchmarkConfig;

public class IntrinsicIncrementalBenchmarkScenario extends
  JastaddAbstractBenchmarkScenario<IntrinsicIncrementalBenchmarkConfig> {

  public IntrinsicIncrementalBenchmarkScenario(final IntrinsicIncrementalBenchmarkConfig bc) throws Exception {
    super(bc, false);
  }

}
