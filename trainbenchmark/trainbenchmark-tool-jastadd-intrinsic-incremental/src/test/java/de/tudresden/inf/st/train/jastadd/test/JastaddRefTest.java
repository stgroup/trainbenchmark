package de.tudresden.inf.st.train.jastadd.test;

import de.tudresden.inf.st.train.jastadd.IntrinsicIncrementalBenchmarkScenario;
import de.tudresden.inf.st.train.jastadd.config.IntrinsicIncrementalBenchmarkConfig;
import de.tudresden.inf.st.train.jastadd.config.IntrinsicIncrementalBenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;
import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkResult;
import hu.bme.mit.trainbenchmark.benchmark.test.TrainBenchmarkTest;

public class JastaddRefTest extends TrainBenchmarkTest {

  @Override
  protected BenchmarkResult runTest(final BenchmarkConfigBase bcb) throws Exception {
    final IntrinsicIncrementalBenchmarkConfig bc = new IntrinsicIncrementalBenchmarkConfigBuilder().setConfigBase(bcb).createConfig();
    final IntrinsicIncrementalBenchmarkScenario scenario = new IntrinsicIncrementalBenchmarkScenario(bc);
    final BenchmarkResult result = scenario.performBenchmark();
    return result;
  }

}
