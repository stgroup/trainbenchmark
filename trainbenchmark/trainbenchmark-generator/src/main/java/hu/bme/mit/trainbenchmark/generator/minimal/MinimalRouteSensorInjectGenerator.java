package hu.bme.mit.trainbenchmark.generator.minimal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import hu.bme.mit.trainbenchmark.constants.Position;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;

import static hu.bme.mit.trainbenchmark.constants.ModelConstants.*;

public class MinimalRouteSensorInjectGenerator extends MinimalModelGenerator {

	public MinimalRouteSensorInjectGenerator(final ModelSerializer<?> serializer, final GeneratorConfig generatorConfig) {
		super(serializer, generatorConfig);
	}

	@Override
	protected void buildPatternModel() throws FileNotFoundException, IOException {
		final Object region = serializer.createVertex(REGION);

		final Object route = serializer.createVertex(ROUTE);
		final Object sensor = serializer.createVertex(SENSOR);

		final Object sw = serializer.createVertex(SWITCH);
		serializer.setAttribute(SWITCH, sw, CURRENTPOSITION, Position.STRAIGHT);

		serializer.createEdge(SENSORS, region, sensor);
		serializer.createEdge(ELEMENTS, region, sw);

		final Map<String, Object> swPOutgoingEdges = ImmutableMap.of(TARGET, sw);
		final Map<String, Object> swPAttributes = ImmutableMap.of(POSITION, Position.STRAIGHT);
		final Object swP = serializer.createVertex(SWITCHPOSITION, swPAttributes, swPOutgoingEdges);
		serializer.createEdge(FOLLOWS, route, swP);

		serializer.createEdge(MONITORED_BY, sw, sensor);
	}

}
