package hu.bme.mit.trainbenchmark.generator.minimal;

import hu.bme.mit.trainbenchmark.constants.Position;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;

import java.io.IOException;

import static hu.bme.mit.trainbenchmark.constants.ModelConstants.*;

public class MinimalSwitchMonitoredRepairGenerator extends MinimalModelGenerator {

	public MinimalSwitchMonitoredRepairGenerator(final ModelSerializer<?> serializer, final GeneratorConfig generatorConfig) {
		super(serializer, generatorConfig);
	}

	@Override
	protected void buildPatternModel() throws IOException {
		final Object region = serializer.createVertex(REGION);
		final Object sw = serializer.createVertex(SWITCH);
		serializer.setAttribute(SWITCH, sw, CURRENTPOSITION, Position.STRAIGHT);

		serializer.createEdge(ELEMENTS, region, sw);
	}

}
