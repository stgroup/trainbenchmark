package hu.bme.mit.trainbenchmark.generator.minimal;

import com.google.common.collect.ImmutableMap;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static hu.bme.mit.trainbenchmark.constants.ModelConstants.*;
import static hu.bme.mit.trainbenchmark.constants.Position.DIVERGING;
import static hu.bme.mit.trainbenchmark.constants.Position.STRAIGHT;
import static hu.bme.mit.trainbenchmark.constants.Signal.GO;

public class MinimalSwitchSetRepairGenerator extends MinimalModelGenerator {

	public MinimalSwitchSetRepairGenerator(final ModelSerializer<?> serializer, final GeneratorConfig generatorConfig) {
		super(serializer, generatorConfig);
	}

	@Override
	protected void buildPatternModel() throws FileNotFoundException, IOException {
		final Object region = serializer.createVertex(REGION);
		final Object segment = serializer.createVertex(SEGMENT);

		serializer.createEdge(ELEMENTS, region, segment);

		// semaphore
		final Map<String, ? extends Object> semaphoreProperties = ImmutableMap.of(SIGNAL, GO);
		final Object semaphore = serializer.createVertex(SEMAPHORE, semaphoreProperties);

		serializer.createEdge(SEMAPHORES, segment, semaphore);

		// route
		final Map<String, Object> routeOutgoingEdges = ImmutableMap.of(ENTRY, semaphore);
		final Map<String, ? extends Object> emptyMap = Collections.emptyMap();
		final Object route = serializer.createVertex(ROUTE, emptyMap, routeOutgoingEdges);

		// sw
		final Object sw = serializer.createVertex(SWITCH);
		serializer.setAttribute(SWITCH, sw, CURRENTPOSITION, DIVERGING);

		serializer.createEdge(ELEMENTS, region, sw);

		// swP
		final Map<String, ? extends Object> swPProperties = ImmutableMap.of(POSITION, STRAIGHT);
		final Map<String, Object> swPOutgoingEdges = ImmutableMap.of(TARGET, sw);
		final Object swP = serializer.createVertex(SWITCHPOSITION, swPProperties, swPOutgoingEdges);
		serializer.createEdge(FOLLOWS, route, swP);
	}

}
