package hu.bme.mit.trainbenchmark.generator.config;

public enum ModelType {

  CONNECTEDSEGMENTS_INJECT("ConnectedSegmentsInject"),
  POSLENGTH_INJECT("PosLengthInject"),
  ROUTESENSOR_INJECT("RouteSensorInject"),
  SEMAPHORENEIGHBOR_INJECT("SemaphoreNeighborInject"),
  SWITCHMONITORED_INJECT("SwitchMonitoredInject"),
  SWITCHSET_INJECT("SwitchSetInject"),

  // repair operations
  CONNECTEDSEGMENTS_REPAIR("ConnectedSegmentsRepair"),
  POSLENGTH_REPAIR("PosLengthRepair"),
  ROUTESENSOR_REPAIR("RouteSensorRepair"),
  SEMAPHORENEIGHBOR_REPAIR("SemaphoreNeighborRepair"),
  SWITCHMONITORED_REPAIR("SwitchMonitoredRepair"),
  SWITCHSET_REPAIR("SwitchSetRepair");


  private String name;

  ModelType(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }

}
