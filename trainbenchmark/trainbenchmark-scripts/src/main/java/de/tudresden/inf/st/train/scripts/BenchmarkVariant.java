package de.tudresden.inf.st.train.scripts;

/**
 * Possible variants to run a benchmark in.
 * Created by rschoene on 10/12/17.
 */
public enum BenchmarkVariant {
  COMBINED,
  INDIVIDUAL_INJECT,
  INDIVIDUAL_REPAIR,
  INDIVIDUAL_INCREMENTAL
}
