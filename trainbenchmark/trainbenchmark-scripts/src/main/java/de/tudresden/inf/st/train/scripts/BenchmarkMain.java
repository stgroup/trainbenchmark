package de.tudresden.inf.st.train.scripts;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.bme.mit.trainbenchmark.benchmark.config.*;
import hu.bme.mit.trainbenchmark.benchmark.result.ResultHelper;
import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkRunner;
import hu.bme.mit.trainbenchmark.config.ExecutionConfig;
import hu.bme.mit.trainbenchmark.constants.RailwayOperation;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Main entry point to start benchmarks.
 * Created by rschoene on 10/12/17.
 */
public class BenchmarkMain {

  @SuppressWarnings("unchecked")
  private static void runBenchmarkSeries(BenchmarkConfigBaseBuilder configBaseBuilder, BenchmarkConfigBuilder configBuilder,
                                         ExecutionConfig ec, ModelSetConfig modelSetConfig, BenchmarkSettingsBasics bbs) {
    Map<String, String> jvmSettingsMap = Utils.createJvmSettingsMap(bbs);
    String jvmDefaultSettings = jvmSettingsMap.getOrDefault("*",
      "-Xms{Xms} -Xmx{Xmx} -server -Xverify:none");
    try {
      for (int size = modelSetConfig.getMinSize(); size <= modelSetConfig.getMaxSize(); size *= 2) {
        String modelFilename = "railway-" + modelSetConfig.getModelVariant() + "-" + size;

        System.out.println("------------------------------------------------------------");
        System.out.println("Model: " + modelFilename);
        System.out.println("------------------------------------------------------------");

        configBaseBuilder.setModelFilename(modelFilename);
        BenchmarkConfigBase configBase = configBaseBuilder.createConfigBase();
        BenchmarkConfig config = (BenchmarkConfig) configBuilder.setConfigBase(configBase).createConfig();

        if (!bbs.isDryRun()) {
          ResultHelper.prepare(config, ec);
        }
        String jvmSetting = jvmSettingsMap.getOrDefault(config.getToolName(), jvmDefaultSettings);

        int exitValue = BenchmarkRunner.runPerformanceBenchmark(config, ec, jvmSetting);
        if (exitValue != 0) {
          System.out.println("Timeout or error occurred, skipping models for larger sizes. Error code: " + exitValue);
          break;
        }
      }
    } catch (InterruptedException | IOException e) {
      System.out.println("Exception occurred during execution.");
      e.printStackTrace();
    }
  }

  public static void main(String args[]) throws Exception {
    // set benchmark variant from program arguments
    if (args.length == 0) {
      System.err.println("Need to specify one argument identifying the benchmark variant");
      System.exit(1);
    }
    String benchmarkVariant = args[0];

    // read basic settings (min, maxsize, etc.)
    ObjectMapper mapper = Utils.getMapper();
    BenchmarkSettingsBasics bbs = Utils.readFromResource(mapper, "basic-settings.json",
      BenchmarkSettingsBasics.class);
    try {
      BenchmarkSettingsBasics loca_bbs = Utils.readFromResource(mapper, "local-basic-settings.json",
        BenchmarkSettingsBasics.class);
      Utils.updateBasicsWithLocal(bbs, loca_bbs);
    } catch (IOException e) {
      System.out.println("No file 'local-basic-settings.json' found. Using default!");
    }

    // import tools to use
    List<BenchmarkConfigBuilder<? extends BenchmarkConfig, ? extends BenchmarkConfigBuilder<?, ?>>> builders =
      BenchmarkSettingsAvailableTools.getBuilders(bbs.getTools());
    System.out.println(builders);

    // read workloads based on benchmark variant
    String workloadsFilename = "workloads.json";
    BenchmarkSettingsVariantWorkloads bsvw = Utils.readFromResource(mapper, workloadsFilename,
      BenchmarkSettingsVariantWorkloads.class);

    Optional<BenchmarkSettingsWorkloads> workloadsOpt = bsvw.getVariants().stream()
      .filter(w -> w.getName().equals(benchmarkVariant))
      .findFirst();
    if (!workloadsOpt.isPresent()) {
      System.err.println("Could not find workloads for given variant, check " + workloadsFilename
        + " and argument " + benchmarkVariant);
      System.exit(4);
    }
    BenchmarkSettingsWorkloads workloads = workloadsOpt.get();

    // start benchmark
    String now = ResultHelper.getNow();
    ExecutionConfig ec = new ExecutionConfig(4000, 8000, bbs.isDryRun());

    System.out.println("Please remember to stop all other Java processes.");
    System.out.println();
    System.out.println("If in doubt, check with this command:");
    System.out.println("$ ps auxw | grep jav[a]");
    System.out.println();
    System.out.println("If there are other Java processes, use:");
    System.out.println("$ killall -9 java");
    System.out.println();
    System.out.println("############################################################");
    System.out.println("Benchmark parameters:");
    System.out.println("- execution config: " + ec);
    System.out.println(bbs);
//		System.out.println("- range: minSize=" + bbs.getMinSize() +", maxSize=" + bbs.getMaxSize());
//		System.out.println("- timeout: " + bbs.getTimeout());
//		System.out.println("- runs: " + bbs.getRuns());
    System.out.println("############################################################");
    System.out.println();

    workloads.getWorkloads().forEach(workloadConfiguration -> {
      String workloadName = workloadConfiguration.getWorkloadName();
      String modelVariant = workloadConfiguration.getModelVariant();
      List<RailwayOperation> operations = workloadConfiguration.getOperations();
      TransformationChangeSetStrategy strategy = workloadConfiguration.getStrategy();
      int constant = workloadConfiguration.getConstant();
      int queryTransformationCount = workloadConfiguration.getQueryTransformationCount();

      System.out.println("============================================================");
      System.out.println("Workload: " + workloadName);
      System.out.println("============================================================");

      ModelSetConfig modelSetConfig = new ModelSetConfig(modelVariant, bbs.getMinSize(), bbs.getMaxSize());
      BenchmarkConfigBaseBuilder bcbb = new BenchmarkConfigBaseBuilder()
        .setBenchmarkId(now).setTimeout(bbs.getTimeout()).setRuns(bbs.getRuns())
        .setOperations(operations).setWorkload(workloadName)
        .setQueryTransformationCount(queryTransformationCount).setTransformationConstant(constant)
        .setTransformationChangeSetStrategy(strategy);

      builders.forEach(bcb -> runBenchmarkSeries(bcbb, bcb, ec, modelSetConfig, bbs));
    });

//		if (binding.variables.get("reportUrl")) {
//			BenchmarkReporter.reportReady(reportUrl)
//		}
  }

}
