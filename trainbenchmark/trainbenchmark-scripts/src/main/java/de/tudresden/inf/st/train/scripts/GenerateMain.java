package de.tudresden.inf.st.train.scripts;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.bme.mit.trainbenchmark.config.ExecutionConfig;
import hu.bme.mit.trainbenchmark.generator.config.*;
import hu.bme.mit.trainbenchmark.generator.runner.GeneratorRunner;

import java.io.IOException;
import java.util.List;

/**
 * Main entry point to generate models.
 * Created by rschoene on 10/13/17.
 */
public class GenerateMain {
  public static void main(String[] args) throws Exception {
    // read basic settings (min, maxsize, etc.)
    ObjectMapper mapper = Utils.getMapper();
    BenchmarkSettingsBasics bbs = Utils.readFromResource(mapper, "basic-settings.json",
        BenchmarkSettingsBasics.class);
    try {
      BenchmarkSettingsBasics loca_bbs = Utils.readFromResource(mapper, "local-basic-settings.json",
          BenchmarkSettingsBasics.class);
      Utils.updateBasicsWithLocal(bbs, loca_bbs);
    } catch (IOException e) {
      System.out.println("No file 'local-basic-settings.json' found. Using default!");
    }

    // import formats to use
    List<GeneratorConfigBuilder<? extends GeneratorConfig, ? extends GeneratorConfigBuilder<?, ?>>> builders =
        BenchmarkSettingsAvailableFormats.getBuilders(bbs.getFormats());
    System.out.println(builders);
    ExecutionConfig ec = new ExecutionConfig(4000, 8000, bbs.isDryRun());

    // generating the MINIMAL scenario
    for (ModelType modelType : ModelType.values()) {
      builders.forEach(generatorConfigBuilder -> {
        try {
          System.out.println("Scenario: " + Scenario.MINIMAL + ", type: " + modelType);
          GeneratorConfigBase configBase = new GeneratorConfigBaseBuilder()
              .setSize(0).setModelType(modelType).setScenario(Scenario.MINIMAL).createGeneratorConfigBase();
          GeneratorConfig config = generatorConfigBuilder.setConfigBase(configBase).createConfig();
          int exitValue = GeneratorRunner.run(config, ec);
          if (exitValue != 0) {
            System.out.println("Timeout or error occurred. Error code: " + exitValue);
          }
        } catch (Exception e) {
          System.out.println("Exception occurred during execution.");
        }
      });
    }

    // generating the other scenarios
    for (Scenario scenario : new Scenario[]{Scenario.BATCH, Scenario.INJECT, Scenario.REPAIR}) {
      builders.forEach(generatorConfigBuilder -> {
        try {

          for (int size = bbs.getMinSize(); size <= bbs.getMaxSize(); size *= 2) {
            System.out.println("Scenario: " + scenario + ", size: " + size);

            GeneratorConfigBase configBase = new GeneratorConfigBaseBuilder()
                .setSize(size).setScenario(scenario)
                .createGeneratorConfigBase();
            GeneratorConfig config = generatorConfigBuilder.setConfigBase(configBase).createConfig();

            int exitValue = GeneratorRunner.run(config, ec);
            if (exitValue != 0) {
              System.out.println("Timeout or error occurred, skipping models for larger sizes. Error code: "
                  + exitValue);
              break;
            }
          }
        } catch (Exception e) {
          System.out.println("Exception occurred during execution.");
        }
      });
    }


  }

}

/*
import de.tudresden.inf.st.trainbenchmark.generator.json4ag.config.ManualGeneratorConfigBuilder
import de.tudresden.inf.st.trainbenchmark.generator.json4agref.config.JastAddManualRefGeneratorConfigBuilder
import hu.bme.mit.trainbenchmark.config.ExecutionConfig
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfigBase
import hu.bme.mit.trainbenchmark.generator.config.Scenario
import hu.bme.mit.trainbenchmark.generator.emf.config.EmfGeneratorConfigBuilder
import hu.bme.mit.trainbenchmark.generator.graph.tinkerpop.config.TinkerGraphFormat
import hu.bme.mit.trainbenchmark.generator.graph.tinkerpop.config.TinkerGraphGeneratorConfigBuilder
import hu.bme.mit.trainbenchmark.generator.runner.GeneratorRunner

def ec = new ExecutionConfig(4000, 6000)
def minSize = 1
/* def maxSize = 2048 *
def maxSize = 256

	def scenarios = [
		Scenario.BATCH,
		Scenario.INJECT,
		Scenario.REPAIR,
		]

		def formats = [
		new EmfGeneratorConfigBuilder(),
		new TinkerGraphGeneratorConfigBuilder().setGraphFormat(TinkerGraphFormat.GRAPHML),
		new ManualGeneratorConfigBuilder(),
		new JastAddManualRefGeneratorConfigBuilder()
		]

		for (scenario in scenarios) {
		formats.each { generatorConfigBuilder ->
		try {
		for (def size = minSize; size <= maxSize; size *= 2) {
		println("Scenario: ${scenario}, size: ${size}")

		def configBase = new GeneratorConfigBase(scenario, size)
		def config = generatorConfigBuilder.setConfigBase(configBase).createConfig()

		def exitValue = GeneratorRunner.run(config, ec)
		if (exitValue != 0) {
		println "Timeout or error occured, skipping models for larger sizes. Error code: ${exitValue}"
		break
		}
		}
		} catch (all) {
		println "Exception occured during execution."
		}
		}
		}

 */
