package de.tudresden.inf.st.train.scripts;

import java.util.List;

/**
 * List of all workload settings by variant.
 * Created by rschoene on 10/12/17.
 */
public class BenchmarkSettingsVariantWorkloads {
  private List<BenchmarkSettingsWorkloads> variants;

  public List<BenchmarkSettingsWorkloads> getVariants() {
    return variants;
  }

  public void setVariants(List<BenchmarkSettingsWorkloads> variants) {
    this.variants = variants;
  }
}
