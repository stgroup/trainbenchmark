package de.tudresden.inf.st.train.scripts;

import de.tudresden.inf.st.trainbenchmark.generator.jastadd.config.ManualGeneratorConfigBuilder;
import de.tudresden.inf.st.trainbenchmark.generator.jastadd.config.ManualIntrinsicGeneratorConfigBuilder;
import de.tudresden.inf.st.trainbenchmark.generator.jastadd.config.RelASTGeneratorConfigBuilder;
import hu.bme.mit.trainbenchmark.generator.config.*;
import hu.bme.mit.trainbenchmark.generator.emf.config.EmfGeneratorConfigBuilder;
import hu.bme.mit.trainbenchmark.generator.graph.tinkerpop.config.TinkerGraphFormat;
import hu.bme.mit.trainbenchmark.generator.graph.tinkerpop.config.TinkerGraphGeneratorConfigBuilder;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Format settings of a benchmark shared throughout all variants.
 * Created by rschoene on 10/13/17.
 */
public class BenchmarkSettingsAvailableFormats {
  /**
   * Create a map, from projectName to a set of all builders in this project
   *
   * @return a mapping from projectName to all builders
   */
  public static Map<String, List<GeneratorConfigBuilder<? extends GeneratorConfig, ? extends GeneratorConfigBuilder<?, ?>>>>
  getAvailableFormats() {
    // { projectName -> [builder] }
    GeneratorConfigBase dummyConfigBase = new GeneratorConfigBaseBuilder()
        .setScenario(Scenario.BATCH).setSize(1)
        .createGeneratorConfigBase();
    return getAllBuilders().stream().collect(Collectors.toMap(
        builder -> builder.setConfigBase(dummyConfigBase).createConfig().getProjectName(),
        s -> new ArrayList<>(Collections.singletonList(s)),
        (oldValue, newValue) -> {
          oldValue.addAll(newValue);
          return oldValue;
        }));
  }

  private static GeneratorConfigBuilder<? extends GeneratorConfig, ? extends GeneratorConfigBuilder<?, ?>>
  getBuilderByName(String className) {
    Optional<Object> result = Utils.maybeCreateNewInstance(className);
    return (GeneratorConfigBuilder<? extends GeneratorConfig, ? extends GeneratorConfigBuilder<?, ?>>) result.orElse(null);
  }

  private static List<GeneratorConfigBuilder<? extends GeneratorConfig, ? extends GeneratorConfigBuilder<?, ?>>>
  getAllBuilders() {
    return Arrays.asList(
        new EmfGeneratorConfigBuilder(),
        new TinkerGraphGeneratorConfigBuilder().setGraphFormat(TinkerGraphFormat.GRAPHML),
        new ManualGeneratorConfigBuilder(),
        new ManualIntrinsicGeneratorConfigBuilder(),
        new RelASTGeneratorConfigBuilder()
    );
  }

  /**
   * Get all builders with the given names of formats. The format name in the config every builder creates
   * must match one of the given project names.
   *
   * @param formatNames names of formats
   * @return a set of matching generator config builders
   */
  public static List<GeneratorConfigBuilder<? extends GeneratorConfig, ? extends GeneratorConfigBuilder<?, ?>>>
  getBuilders(String... formatNames) {
    Map<String, List<GeneratorConfigBuilder<? extends GeneratorConfig, ? extends GeneratorConfigBuilder<?, ?>>>>
        availableFormats = getAvailableFormats();
    return Arrays.stream(formatNames).map(name -> {
      List<GeneratorConfigBuilder<? extends GeneratorConfig, ? extends GeneratorConfigBuilder<?, ?>>>
          builders = availableFormats.get(name);
      if (builders.isEmpty()) {
        throw new IllegalArgumentException("Could not find format " + name + ", available formats are: "
            + availableFormats.keySet());
      }
      return builders;
    }).reduce((set1, set2) -> {
      set1.addAll(set2);
      return set1;
    }).orElse(Collections.emptyList());
  }
}
