package de.tudresden.inf.st.train.scripts;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Utility methods for the main entry points.
 * Created by rschoene on 10/13/17.
 */
public class Utils {
  static Map<String, String> createJvmSettingsMap(BenchmarkSettingsBasics bbs) {
    Map<String, String> result = new HashMap<>();
    for (BenchmarkSettingsBasics.JVMSetting setting : bbs.getJvmSettings()) {
      for (String toolName : setting.getApplyTo()) {
        result.put(toolName, setting.getArgs());
      }
    }
    return result;
  }

  private static File readFromResource(String filename) throws IOException {
    URL basicSettingsURL = BenchmarkMain.class.getClassLoader().getResource(filename);
    if (basicSettingsURL == null) {
      System.err.println();
      throw new IOException("Could not access " + filename + ". Exiting.");
    }
    return new File(basicSettingsURL.getFile());
  }

  static <T> T readFromResource(ObjectMapper mapper, String filename, Class<T> clazz) throws IOException {
    File basicSettingsFile = readFromResource(filename);
    T result = null;
    try {
      result = mapper.readValue(basicSettingsFile, clazz);
    } catch (Exception e) {
      System.err.println("Could not load '" + filename + "'. Exiting.");
      e.printStackTrace();
      System.exit(2);
    }
    return result;
  }

  static void updateBasicsWithLocal(BenchmarkSettingsBasics base, BenchmarkSettingsBasics local)
    throws IllegalStateException {
    List<Method> setter = Arrays.stream(BenchmarkSettingsBasics.class.getMethods())
      .filter(m -> m.getName().startsWith("set"))
      .collect(Collectors.toList());
    for (Method method : BenchmarkSettingsBasics.class.getMethods()) {
      final String name = method.getName().startsWith("get") ?
        method.getName().substring(3) :
        (method.getName().startsWith("is") ? method.getName().substring(2) : null);
      if (name == null) {
        continue;
      }
      try {
        Object result = method.invoke(local);
        if (result != null) {
          Optional<Method> matchingSetter = setter.stream()
            .filter(m -> m.getName().endsWith(name))
            .findFirst();
          if (matchingSetter.isPresent()) {
            matchingSetter.get().invoke(base, result);
          }
        }
        // now do sanity check if set in updated base
        result = method.invoke(base);
        if (result == null) {
          throw new IllegalStateException("Basic setting for " + name + " is not given.");
        }
      } catch (IllegalAccessException | InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }

  static ObjectMapper getMapper() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
    return mapper;
  }

  static Optional<Object> maybeCreateNewInstance(final String className) {
    try {
      return Optional.of(Class.forName(className).newInstance());
    } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
      System.err.println("Could not find builder class '" + className + "'");
      e.printStackTrace();
      return Optional.empty();
    }
  }
}
