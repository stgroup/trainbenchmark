package de.tudresden.inf.st.train.scripts;

import java.util.List;

/**
 * Workload settings bean.
 * Created by rschoene on 10/12/17.
 */
public class BenchmarkSettingsWorkloads {
  private String name;
  private List<BenchmarkSettingsWorkload> workloads;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<BenchmarkSettingsWorkload> getWorkloads() {
    return workloads;
  }

  public void setWorkloads(List<BenchmarkSettingsWorkload> workloads) {
    this.workloads = workloads;
  }
}
