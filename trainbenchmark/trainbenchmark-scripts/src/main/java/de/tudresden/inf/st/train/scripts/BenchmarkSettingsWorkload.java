package de.tudresden.inf.st.train.scripts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hu.bme.mit.trainbenchmark.benchmark.config.TransformationChangeSetStrategy;
import hu.bme.mit.trainbenchmark.constants.RailwayOperation;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Benchmark setting workload bean.
 * Created by rschoene on 10/12/17.
 */
public class BenchmarkSettingsWorkload {
  private String workloadName;
  private String modelVariant;
  private List<RailwayOperation> operations;
  private TransformationChangeSetStrategy strategy;
  //	private String strategy;
  private int constant;
  private int queryTransformationCount;
  @JsonIgnore
  private Map<String, RailwayOperation> railwayOps;

  public BenchmarkSettingsWorkload() {
    railwayOps = Arrays.stream(RailwayOperation.values()).collect(Collectors.toMap(
        RailwayOperation::toString, Function.identity()
    ));
  }

  public String getWorkloadName() {
    return workloadName;
  }

  public void setWorkloadName(String workloadName) {
    this.workloadName = workloadName;
  }

  public String getModelVariant() {
    return modelVariant;
  }

  public void setModelVariant(String modelVariant) {
    this.modelVariant = modelVariant;
  }

  public List<RailwayOperation> getOperations() {
    return operations;
  }

  public void setOperations(List<String> operations) {
    this.operations = operations.stream().map(op -> railwayOps.get(op)).collect(Collectors.toList());
  }

  public TransformationChangeSetStrategy getStrategy() {
    return strategy;
  }

  public void setStrategy(TransformationChangeSetStrategy strategy) {
    this.strategy = strategy;
  }

  public int getConstant() {
    return constant;
  }

  public void setConstant(int constant) {
    this.constant = constant;
  }

  public int getQueryTransformationCount() {
    return queryTransformationCount;
  }

  public void setQueryTransformationCount(int queryTransformationCount) {
    this.queryTransformationCount = queryTransformationCount;
  }
}
