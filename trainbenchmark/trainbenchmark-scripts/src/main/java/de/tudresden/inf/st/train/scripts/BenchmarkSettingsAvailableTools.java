package de.tudresden.inf.st.train.scripts;

import de.tudresden.inf.st.train.jastadd.config.*;
import hu.bme.mit.trainbenchmark.benchmark.config.*;
import hu.bme.mit.trainbenchmark.benchmark.tinkergraph.config.TinkerGraphBenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.benchmark.viatra.config.ViatraBackend;
import hu.bme.mit.trainbenchmark.benchmark.viatra.config.ViatraBenchmarkConfigBuilder;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Tool settings of a benchmark shared throughout all variants
 * Created by rschoene on 10/12/17.
 */
public class BenchmarkSettingsAvailableTools {
  public static Map<String, BenchmarkConfigBuilder<? extends BenchmarkConfig, ? extends BenchmarkConfigBuilder<?, ?>>>
  getAvailableTools() {
    // { toolName -> builder }
    BenchmarkConfigBase dummyConfigBase = new BenchmarkConfigBaseBuilder()
        .setBenchmarkId("").setTimeout(0L).setRuns(0).setModelFilename("")
        .setOperations(new ArrayList<>()).setWorkload("")
        .setTransformationChangeSetStrategy(TransformationChangeSetStrategy.FIXED)
        .setQueryTransformationCount(0).setTransformationConstant(0)
        .createConfigBase();
    return getAllBuilders().stream().collect(Collectors.toMap(
        builder -> builder.setConfigBase(dummyConfigBase).createConfig().getToolName(),
        Function.identity()));
  }

  private static BenchmarkConfigBuilder<? extends BenchmarkConfig, ? extends BenchmarkConfigBuilder<?, ?>>
  getBuilderByName(String className) {
    Optional<Object> result = Utils.maybeCreateNewInstance(className);
    return (BenchmarkConfigBuilder<? extends BenchmarkConfig, ? extends BenchmarkConfigBuilder<?, ?>>) result.orElse(null);
  }

  private static List<BenchmarkConfigBuilder<? extends BenchmarkConfig, ? extends BenchmarkConfigBuilder<?, ?>>>
  getAllBuilders() {
    return Arrays.asList(
        new NameLookupBenchmarkConfigBuilder(),
        new NameLookupIncrementalBenchmarkConfigBuilder(),
        new IntrinsicBenchmarkConfigBuilder(),
        new IntrinsicIncrementalBenchmarkConfigBuilder(),
        new RelASTBenchmarkConfigBuilder(),
        new RelASTIncrementalBenchmarkConfigBuilder(),
        new ManualRelastBenchmarkConfigBuilder(),
        new ManualRelastIncrementalBenchmarkConfigBuilder(),
        new TinkerGraphBenchmarkConfigBuilder(),
        new ViatraBenchmarkConfigBuilder().setBackend(ViatraBackend.INCREMENTAL),
        new ViatraBenchmarkConfigBuilder().setBackend(ViatraBackend.LOCAL_SEARCH)
    );
  }

  /**
   * Get all builders with the given names of tools. The tool name in the config every builder creates must match one
   * of the given tool names.
   *
   * @param toolNames names of tools
   * @return a list of matching benchmark config builders
   */
  public static List<BenchmarkConfigBuilder<? extends BenchmarkConfig, ? extends BenchmarkConfigBuilder<?, ?>>>
  getBuilders(String... toolNames) {
    Map<String, BenchmarkConfigBuilder<? extends BenchmarkConfig, ? extends BenchmarkConfigBuilder<?, ?>>> availableTools = getAvailableTools();
    return Arrays.stream(toolNames).map(name -> {
      BenchmarkConfigBuilder<? extends BenchmarkConfig, ? extends BenchmarkConfigBuilder<?, ?>> builder = availableTools.get(name);
      if (builder == null) {
        throw new IllegalArgumentException("Could not find tool " + name + ", available tools are: " + availableTools.keySet());
      }
      return builder;
    }).collect(Collectors.toList());
  }
}
