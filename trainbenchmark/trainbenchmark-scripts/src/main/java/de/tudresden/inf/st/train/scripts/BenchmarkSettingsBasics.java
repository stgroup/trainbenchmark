package de.tudresden.inf.st.train.scripts;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.Arrays;
import java.util.List;

/**
 * Basic settings bean of a benchmark shared throughout all variants.
 * Created by rschoene on 10/12/17.
 */
public class BenchmarkSettingsBasics {

  @JsonInclude(JsonInclude.Include.NON_DEFAULT)
  private Integer minSize = null;
  @JsonInclude(JsonInclude.Include.NON_DEFAULT)
  private Integer maxSize = null;
  @JsonInclude(JsonInclude.Include.NON_DEFAULT)
  private Long timeout = null;
  @JsonInclude(JsonInclude.Include.NON_DEFAULT)
  private Integer runs = null;
  @JsonInclude(JsonInclude.Include.NON_DEFAULT)
  private Boolean dryRun = null;
  private String[] tools;
  private String[] formats;
  private List<JVMSetting> jvmSettings;

  /**
   * Minimum model size
   */
  public Integer getMinSize() {
    return minSize;
  }

  public void setMinSize(int minSize) {
    this.minSize = minSize;
  }

  /**
   * Maximum model size
   */
  public Integer getMaxSize() {
    return maxSize;
  }

  public void setMaxSize(int maxSize) {
    this.maxSize = maxSize;
  }

  /**
   * Timeout in seconds for each single run.
   */
  public Long getTimeout() {
    return timeout;
  }

  public void setTimeout(long timeout) {
    this.timeout = timeout;
  }

  /**
   * Number of runs per tool-workload combination.
   */
  public Integer getRuns() {
    return runs;
  }

  public void setRuns(int runs) {
    this.runs = runs;
  }

  /**
   * Don't run any Java processes.
   */
  public Boolean isDryRun() {
    return dryRun;
  }

  @JsonSetter("dry-run")
  public void setDryRun(boolean dryRun) {
    this.dryRun = dryRun;
  }

  /**
   * List of tool names to use
   */
  public String[] getTools() {
    return tools;
  }

  public void setTools(String[] tools) {
    this.tools = tools;
  }

  /**
   * List of format names to use
   */
  public String[] getFormats() {
    return formats;
  }

  public void setFormats(String[] formats) {
    this.formats = formats;
  }

  public List<JVMSetting> getJvmSettings() {
    return jvmSettings;
  }

  public void setJvmSettings(List<JVMSetting> jvmSettings) {
    this.jvmSettings = jvmSettings;
  }

  @Override
  public String toString() {
    return "BenchmarkSettingsBasics{" +
      "minSize=" + minSize +
      ", maxSize=" + maxSize +
      ", timeout=" + timeout +
      ", runs=" + runs +
      ", dryRun=" + dryRun +
      ", tools=" + Arrays.toString(tools) +
      '}';
  }

  static class JVMSetting {
    private List<String> applyTo;
    private String args;

    public List<String> getApplyTo() {
      return applyTo;
    }

    public void setApplyTo(List<String> applyTo) {
      this.applyTo = applyTo;
    }

    public String getArgs() {
      return args;
    }

    public void setArgs(String args) {
      this.args = args;
    }
  }
}
