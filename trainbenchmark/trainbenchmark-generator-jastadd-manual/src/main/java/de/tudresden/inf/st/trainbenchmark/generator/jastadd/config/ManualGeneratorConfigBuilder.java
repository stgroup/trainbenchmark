/**
 * Created by rschoene on 5/11/17.
 */
package de.tudresden.inf.st.trainbenchmark.generator.jastadd.config;

import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfigBuilder;

public class ManualGeneratorConfigBuilder
  extends GeneratorConfigBuilder<ManualGeneratorConfig, ManualGeneratorConfigBuilder> {

  @Override
  public ManualGeneratorConfig createConfig() {
    checkNotNulls();
    return new ManualGeneratorConfig(configBase);
  }
}
