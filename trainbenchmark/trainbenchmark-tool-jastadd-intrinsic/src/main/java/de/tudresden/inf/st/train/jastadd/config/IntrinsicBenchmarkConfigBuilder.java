package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBuilder;

public class IntrinsicBenchmarkConfigBuilder extends BenchmarkConfigBuilder<IntrinsicBenchmarkConfig, IntrinsicBenchmarkConfigBuilder> {

  @Override
  public IntrinsicBenchmarkConfig createConfig() {
    checkNotNulls();
    return new IntrinsicBenchmarkConfig(configBase);
  }

}
