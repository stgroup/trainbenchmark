package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.IntrinsicBenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;

public class IntrinsicBenchmarkMain {
  public static void main(final String[] args) throws Exception {
    final IntrinsicBenchmarkConfig bc = BenchmarkConfig.fromFile(args[0], IntrinsicBenchmarkConfig.class);
    final IntrinsicBenchmarkScenario scenario = new IntrinsicBenchmarkScenario(bc);
    scenario.performBenchmark();
    scenario.printTraces();
  }
}
