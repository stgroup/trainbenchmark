package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.IntrinsicBenchmarkConfig;

public class IntrinsicBenchmarkScenario extends
  JastaddAbstractBenchmarkScenario<IntrinsicBenchmarkConfig> {

  public IntrinsicBenchmarkScenario(final IntrinsicBenchmarkConfig bc) throws Exception {
    super(bc, true);
  }

}
