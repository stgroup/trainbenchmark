package de.tudresden.inf.st.train.jastadd.transformations.inject;

import de.tudresden.inf.st.train.jastadd.ast.Opt;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSemaphoreNeighborInjectMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationInjectSemaphoreNeighbor<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddSemaphoreNeighborInjectMatch, TJastaddDriver> {

  public JastaddTransformationInjectSemaphoreNeighbor(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddSemaphoreNeighborInjectMatch> matches) {
    for (final JastaddSemaphoreNeighborInjectMatch match : matches) {
      match.getRoute().setEntryOpt(new Opt<>());
    }
    driver.flushCache();
  }

}
