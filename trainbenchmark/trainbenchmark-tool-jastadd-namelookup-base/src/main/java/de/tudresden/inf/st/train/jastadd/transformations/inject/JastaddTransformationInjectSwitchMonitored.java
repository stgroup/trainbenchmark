package de.tudresden.inf.st.train.jastadd.transformations.inject;

import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import de.tudresden.inf.st.train.jastadd.ast.TrackElementRef;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchMonitoredInjectMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.ArrayList;
import java.util.Collection;

public class JastaddTransformationInjectSwitchMonitored<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddSwitchMonitoredInjectMatch, TJastaddDriver> {

  public JastaddTransformationInjectSwitchMonitored(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddSwitchMonitoredInjectMatch> matches) {
    java.util.List<TrackElementRef> refsToBeRemoved = new ArrayList<>();

    for (final JastaddSwitchMonitoredInjectMatch match : matches) {
      for (final Sensor sensor : match.getSw().monitoredBy()) {
        for (TrackElementRef trackElementRef : sensor.getMonitoredElements()) {
          if (trackElementRef.getTrackElement() == match.getSw()) {
            refsToBeRemoved.add(trackElementRef);
          }
        }
      }
    }

    for(TrackElementRef ref : refsToBeRemoved) {
      ref.removeSelf();
    }

    driver.flushCache();
  }

}
