package de.tudresden.inf.st.train.jastadd.transformations.inject;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddPosLengthInjectMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationInjectPosLength<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddPosLengthInjectMatch, TJastaddDriver> {

  public JastaddTransformationInjectPosLength(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddPosLengthInjectMatch> matches) {
    for (final JastaddPosLengthInjectMatch match : matches) {
      match.getSegment().setLength(0);
    }
    driver.flushCache();
  }

}
