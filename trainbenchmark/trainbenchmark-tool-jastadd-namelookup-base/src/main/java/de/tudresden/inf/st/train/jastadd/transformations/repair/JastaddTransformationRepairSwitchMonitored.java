package de.tudresden.inf.st.train.jastadd.transformations.repair;

import de.tudresden.inf.st.train.jastadd.ast.List;
import de.tudresden.inf.st.train.jastadd.ast.SwitchRef;
import de.tudresden.inf.st.train.jastadd.ast.TrackElementRef;
import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchMonitoredMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationRepairSwitchMonitored<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddSwitchMonitoredMatch, TJastaddDriver> {

  public JastaddTransformationRepairSwitchMonitored(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddSwitchMonitoredMatch> matches) {
    for (final JastaddSwitchMonitoredMatch ssnm : matches) {

      int id = driver.nextId();
      List<TrackElementRef> refList = new List<>();
      refList.add(ssnm.getSw().createRef());

      Sensor sensor = new Sensor(id, refList);

      ssnm.getSw().containingRegion().getSensorList().add(sensor);
    }
    driver.flushCache();
  }

}
