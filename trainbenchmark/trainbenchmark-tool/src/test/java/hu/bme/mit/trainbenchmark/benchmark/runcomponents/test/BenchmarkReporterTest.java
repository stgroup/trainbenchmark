package hu.bme.mit.trainbenchmark.benchmark.runcomponents.test;

import java.io.IOException;
import java.net.SocketException;

import org.junit.Ignore;
import org.junit.Test;

import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkReporter;

public class BenchmarkReporterTest {

	@Ignore
	@Test
	public void test() throws SocketException, IOException {
		final String reportUrl = "https://hooks.slack.com/services/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";
		BenchmarkReporter.reportReady(reportUrl);
	}

}
