package hu.bme.mit.trainbenchmark.benchmark.result;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.config.ExecutionConfig;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public final class ResultHelper {

	public static final String WORKSPACE_DIR = "../";
	public static final String RESULT_DIR = "results/";
	public static final String TOOLS_DIR = "tools/";
	public static final String DIAGRAM_DIR = "diagrams/";
	public static final String SCRIPT_PATH = WORKSPACE_DIR + "trainbenchmark-scripts/src/";
	public static final String BENCHMARK_SCRIPT = "BenchmarkScript.groovy";

	/**
	 * Note that this method should be called from the workspace directory.
	 *
	 * @return
	 */

	public static int getNextId() {
		final File[] resultDirs = new File(WORKSPACE_DIR + RESULT_DIR).listFiles(File::isDirectory);
		final Integer lastId = Arrays.stream(resultDirs) //
				.map(dir -> dir.getName()) //
				.filter(name -> StringUtils.isNumeric(name)) //
				.map(name -> Integer.parseInt(name)) //
				.max(Integer::compare) //
				.orElse(0);
		final Integer newId = lastId + 1;
		return newId;
	}

	public static String getResultDirForId(int id) {
		return RESULT_DIR + String.format("%04d/", id);
	}

	public static String getDiagramDirForId(int id) {
		return DIAGRAM_DIR + String.format("%04d/", id);
	}

	public static String getRelativeResultPathForId(int id) {
		return WORKSPACE_DIR + getResultDirForId(id);
	}

	public static void createResultDirs(int id) {
		final String dir = WORKSPACE_DIR + getResultDirForId(id);
		final String diagramDir = WORKSPACE_DIR + getDiagramDirForId(id);
		System.out.println("Creating dirs: " + dir);
		if (new File(dir).mkdir()) {
			// change/create symlink for recent
			Path recent = Paths.get(WORKSPACE_DIR, RESULT_DIR, "recent");
			try {
				Files.deleteIfExists(recent);
				Files.createSymbolicLink(recent, Paths.get(dir));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("Could not create dir " + dir);
		}
		if (new File(diagramDir).mkdir()) {
			// change/create symlink for recent
			Path recent = Paths.get(WORKSPACE_DIR, DIAGRAM_DIR, "recent");
			try {
				Files.deleteIfExists(recent);
				Files.createSymbolicLink(recent, Paths.get(diagramDir));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("Could not create dir " + dir);
		}
	}

	public static int createNewResultDir() {
		int id = getNextId();
		createResultDirs(id);
		return id;
	}

	public static void saveConfiguration(int id, String fileName) throws IOException {
		final File srcFile = new File(SCRIPT_PATH + fileName);
		final File destFile = new File(getRelativeResultPathForId(id) + BENCHMARK_SCRIPT);
		FileUtils.copyFile(srcFile, destFile);
	}

	public static void saveConfiguration(int id) throws IOException {
		final File srcFile = new File(SCRIPT_PATH + BENCHMARK_SCRIPT);
		final File destFile = new File(getRelativeResultPathForId(id) + BENCHMARK_SCRIPT);
		FileUtils.copyFile(srcFile, destFile);
	}

	public static void createNewResultDirs(Collection<BenchmarkConfigBuilder<? extends BenchmarkConfig, ? extends BenchmarkConfigBuilder<?, ?>>> tools) {
	}

	/**
	 * Prepare a benchmark run, i.e., creates necessary directories. This operation is idempotent.
	 * @param config the benchmark configuration
	 * @param ec the execution configuration
	 */
	public static void prepare(BenchmarkConfig config, ExecutionConfig ec) {
		// ensure directory for the tool
		File toolDir = getToolDirectory(config.getProjectName());
		toolDir.mkdir();
		// ensure directory for the run inside toolDir
		File runDir = getResultDirectory(toolDir, config.getConfigBase().getBenchmarkId());
		runDir.mkdir();
	}

	public static String getNow() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		return dateFormat.format(new Date());
	}

	public static File getToolDirectory(String projectName) {
		return FileUtils.getFile(WORKSPACE_DIR, RESULT_DIR, TOOLS_DIR, projectName);
	}

	public static File getResultDirectory(File toolDirectory, String benchmarkId) {
		return FileUtils.getFile(toolDirectory, "run-" + benchmarkId);
	}

	public static File getResultDirectory(String projectName, String benchmarkId) {
		return getResultDirectory(getToolDirectory(projectName), benchmarkId);
	}

}
