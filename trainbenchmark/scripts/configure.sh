#!/bin/bash

cd "$( cd "$( dirname "$0" )" && pwd )/../"||exit 1

MINSIZE="$1"
MAXSIZE="$2"
TIMEOUT="$3"
RUNS="$4"

if [ -z "$RUNS" ]
then
    echo "Usage: ./scripts/configure.sh min max timeout runs"
    echo "Will configure the generateor and benchmark to use problems between sizes min and max"
    echo "The benchmark receives a timeout in seconds and the amount of runs"
    echo "Default is: ./scripts/configure.sh 1 512 900 10"
    exit 1
fi
echo "Configuring the source with:"
echo "minSize: $MINSIZE"
echo "maxSize: $MAXSIZE"
echo "timeout: $TIMEOUT"
echo "runs: $RUNS"
echo -e "\\033[1;96mRemember that this only configures and initializes. Further steps:"
echo -e "\\033[1;91mGenerate:"
echo -e "\\033[0m  if not yet done run ./gradlew generate"
echo "     but beware: maxSize 1024 -> ~40GB of data"
echo -e "\\033[1;91mBenchmark:"
echo -e "\\033[0m  ./gradlew individualInjectBenchmark"
echo "  ./gradlew individualRepairBenchmark"

cat <<EOF > ./trainbenchmark-scripts/src/main/resources/local-basic-settings.json
{
  "minSize": $MINSIZE,
  "maxSize": $MAXSIZE,
  "timeout": $TIMEOUT,
  "runs": $RUNS,
  "dry-run": false
}
EOF
