package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;

public class NameLookupIncrementalBenchmarkConfig extends JastaddAbstractBenchmarkConfig {

  protected NameLookupIncrementalBenchmarkConfig(final BenchmarkConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getToolName() {
    return "Name Lookup (Incremental)";
  }

  @Override
  public String getProjectName() {
    return "jastadd-namelookup-incremental";
  }

}
