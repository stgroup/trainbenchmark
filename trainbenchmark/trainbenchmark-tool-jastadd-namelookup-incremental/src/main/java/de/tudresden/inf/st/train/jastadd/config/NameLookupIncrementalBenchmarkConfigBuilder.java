package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBuilder;

public class NameLookupIncrementalBenchmarkConfigBuilder extends BenchmarkConfigBuilder<NameLookupIncrementalBenchmarkConfig, NameLookupIncrementalBenchmarkConfigBuilder> {

  @Override
  public NameLookupIncrementalBenchmarkConfig createConfig() {
    checkNotNulls();
    return new NameLookupIncrementalBenchmarkConfig(configBase);
  }

}
