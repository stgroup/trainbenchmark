package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.NameLookupIncrementalBenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;

public class NameLookupIncrementalBenchmarkMain {
  public static void main(final String[] args) throws Exception {
    final NameLookupIncrementalBenchmarkConfig bc = BenchmarkConfig.fromFile(args[0], NameLookupIncrementalBenchmarkConfig.class);
    final NameLookupIncrementalBenchmarkScenario scenario = new NameLookupIncrementalBenchmarkScenario(bc);
    scenario.performBenchmark();
    scenario.printTraces();
  }
}
