package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.NameLookupIncrementalBenchmarkConfig;

public class NameLookupIncrementalBenchmarkScenario extends
  JastaddAbstractBenchmarkScenario<NameLookupIncrementalBenchmarkConfig> {

  public NameLookupIncrementalBenchmarkScenario(final NameLookupIncrementalBenchmarkConfig bc) throws Exception {
    super(bc, false);
  }

}
