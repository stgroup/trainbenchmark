package de.tudresden.inf.st.train.jastadd.test;

import de.tudresden.inf.st.train.jastadd.ast.Position;
import de.tudresden.inf.st.train.jastadd.ast.RailwayContainer;
import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import de.tudresden.inf.st.train.jastadd.ast.Switch;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Collection;

public class IncrementalSwitchMonitoredInjectTest {

  @Test
  public void testIncrementalCollection() throws IOException {

    JastaddDriver driver = JastaddDriver.create(false, null);

    RailwayContainer model = null;

    driver.read("../models/railway-minimal-SwitchMonitoredInject-jastadd-manual.json");
    model = driver.getModel();
    Switch beforeSwitch = new Switch();
    beforeSwitch.setCurrentPosition(Position.STRAIGHT);

    Switch afterSwitch = new Switch();
    afterSwitch.setCurrentPosition(Position.STRAIGHT);

    org.junit.Assert.assertNotNull(model);

    // first, find switch monitored inject match
    Collection<Switch> switches = model.switchMonitoredInjectMatches();
    Assert.assertEquals(1, switches.size());

    model.getRegion(0).getTrackElementList().insertChild(beforeSwitch, 0);
    model.getRegion(0).getTrackElementList().add(afterSwitch);

    // assert that we have three switches and one sensor
    Assert.assertEquals(1, model.getNumRegion());
    Assert.assertNotNull(model.getRegion(0));
    Assert.assertEquals(3, model.getRegion(0).getNumTrackElement());
    Assert.assertTrue(model.getRegion(0).getTrackElement(0).isSwitch());
    Assert.assertTrue(model.getRegion(0).getTrackElement(1).isSwitch());
    Assert.assertTrue(model.getRegion(0).getTrackElement(2).isSwitch());

    Assert.assertEquals(beforeSwitch, model.getRegion(0).getTrackElement(0));
    Assert.assertEquals(afterSwitch, model.getRegion(0).getTrackElement(2));

    Switch monitoredSwitch = model.getRegion(0).getTrackElement(1).asSwitch();
    Assert.assertNotNull(monitoredSwitch);

    Assert.assertEquals(1, model.getRegion(0).getNumSensor());
    Sensor sensor = model.getRegion(0).getSensor(0);

    // first, assert monitoredBy and monitors relation
    Assert.assertEquals(1, sensor.getNumMonitoredElement());
    Assert.assertEquals(1, sensor.monitors().size());
    Assert.assertEquals(monitoredSwitch, sensor.getMonitoredElement(0).getSwitch());
    Assert.assertEquals(1, monitoredSwitch.monitoredBy().size());
    Assert.assertEquals(sensor, monitoredSwitch.monitoredBy().get(0));

    //next, remove monitored by relation
    sensor.getMonitoredElementList().removeChild(0);

    // check if relation is removed
    Assert.assertEquals(0, sensor.getNumMonitoredElement());
    Assert.assertEquals(0, sensor.monitors().size());
    Assert.assertEquals(0, monitoredSwitch.monitoredBy().size());

    // remove the switch
    model.getRegion(0).getTrackElementList().removeChild(0);

    // next, assert there are 2 inject matches
    switches = model.switchMonitoredInjectMatches();
    Assert.assertEquals(2, switches.size());

    // remove the sensor
    model.getRegion(0).getSensorList().removeChild(0);
    Assert.assertEquals(0, model.getRegion(0).getNumSensor());

    // next, assert there are 2 inject matches
    switches = model.switchMonitoredInjectMatches();
    Assert.assertEquals(2, switches.size());

  }

}
