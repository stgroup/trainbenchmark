package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Segment;
import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import hu.bme.mit.trainbenchmark.benchmark.matches.ConnectedSegmentsMatch;

public class JastaddConnectedSegmentsMatch extends JastaddMatch implements ConnectedSegmentsMatch {

  protected final Sensor sensor;
  protected final Segment segment1;
  protected final Segment segment2;
  protected final Segment segment3;
  protected final Segment segment4;
  protected final Segment segment5;
  protected final Segment segment6;

  public JastaddConnectedSegmentsMatch(final Sensor sensor, final Segment segment1, final Segment segment2,
                                       final Segment segment3, final Segment segment4, final Segment segment5, final Segment segment6) {
    super();
    this.sensor = sensor;
    this.segment1 = segment1;
    this.segment2 = segment2;
    this.segment3 = segment3;
    this.segment4 = segment4;
    this.segment5 = segment5;
    this.segment6 = segment6;
  }

  @Override
  public Sensor getSensor() {
    return sensor;
  }

  @Override
  public Segment getSegment1() {
    return segment1;
  }

  @Override
  public Segment getSegment2() {
    return segment2;
  }

  @Override
  public Segment getSegment3() {
    return segment3;
  }

  @Override
  public Segment getSegment4() {
    return segment4;
  }

  @Override
  public Segment getSegment5() {
    return segment5;
  }

  @Override
  public Segment getSegment6() {
    return segment6;
  }

}
