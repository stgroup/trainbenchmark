package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSemaphoreNeighborMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.Collection;

public class JastaddQuerySemaphoreNeighbor<TJastaddDriver extends JastaddDriver> extends JastaddQuery<JastaddSemaphoreNeighborMatch> {

  public JastaddQuerySemaphoreNeighbor(final TJastaddDriver driver) {
    super(RailwayQuery.SEMAPHORENEIGHBOR, driver);
  }

  @Override
  public Collection<JastaddSemaphoreNeighborMatch> evaluate() {

    return driver.getModel().semaphoreNeighborMatches();
  }
}
