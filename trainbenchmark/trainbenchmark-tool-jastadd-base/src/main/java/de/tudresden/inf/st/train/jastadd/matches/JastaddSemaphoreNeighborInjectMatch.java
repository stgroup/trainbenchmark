package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Route;
import de.tudresden.inf.st.train.jastadd.ast.Semaphore;
import hu.bme.mit.trainbenchmark.benchmark.matches.SemaphoreNeighborInjectMatch;

public class JastaddSemaphoreNeighborInjectMatch extends JastaddMatch implements SemaphoreNeighborInjectMatch {

  protected final Route route;
  protected final Semaphore semaphore;

  public JastaddSemaphoreNeighborInjectMatch(final Route route, final Semaphore semaphore) {
    super();
    this.route = route;
    this.semaphore = semaphore;
  }

  @Override
  public Route getRoute() {
    return route;
  }

  @Override
  public Semaphore getSemaphore() {
    return semaphore;
  }

}
