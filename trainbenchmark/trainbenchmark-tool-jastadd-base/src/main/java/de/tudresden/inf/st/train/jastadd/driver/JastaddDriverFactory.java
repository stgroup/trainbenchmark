package de.tudresden.inf.st.train.jastadd.driver;

import de.tudresden.inf.st.train.jastadd.ast.ASTState;
import hu.bme.mit.trainbenchmark.benchmark.driver.DriverFactory;

public class JastaddDriverFactory extends DriverFactory<JastaddDriver> {

  protected final boolean flushCaches;

  public ASTState.Trace.Receiver getReceiver() {
    return receiver;
  }

  public void setReceiver(ASTState.Trace.Receiver receiver) {
    this.receiver = receiver;
  }

  private ASTState.Trace.Receiver receiver;

  public JastaddDriverFactory(boolean flushCaches) {
    super();
    this.flushCaches = flushCaches;
  }


  @Override
  public JastaddDriver createInstance() throws Exception {
    return new JastaddDriver(flushCaches, receiver);
  }

}
