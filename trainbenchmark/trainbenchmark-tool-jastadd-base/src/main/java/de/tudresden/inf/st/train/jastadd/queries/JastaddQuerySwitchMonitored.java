package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.ast.Switch;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchMonitoredMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.ArrayList;
import java.util.Collection;

public class JastaddQuerySwitchMonitored<TJastaddDriver extends JastaddDriver> extends
  JastaddQuery<JastaddSwitchMonitoredMatch> {

  public JastaddQuerySwitchMonitored(final TJastaddDriver driver) {
    super(RailwayQuery.SWITCHMONITORED, driver);
  }

  @Override
  public Collection<JastaddSwitchMonitoredMatch> evaluate() {
    final Collection<JastaddSwitchMonitoredMatch> matches = new ArrayList<>();

    for (final Switch sw : driver.getModel().switchMonitoredMatches()) {
      matches.add(new JastaddSwitchMonitoredMatch(sw));
    }

    return matches;
  }
}
