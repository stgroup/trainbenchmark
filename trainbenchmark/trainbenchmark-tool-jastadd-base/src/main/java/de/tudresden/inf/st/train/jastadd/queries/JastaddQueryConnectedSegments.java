package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddConnectedSegmentsMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class JastaddQueryConnectedSegments<TJastaddDriver extends JastaddDriver>
  extends JastaddQuery<JastaddConnectedSegmentsMatch> {

  public JastaddQueryConnectedSegments(final TJastaddDriver driver) {
    super(RailwayQuery.CONNECTEDSEGMENTS, driver);
  }

  @Override
  public Collection<JastaddConnectedSegmentsMatch> evaluate() throws IOException {
    return driver.getModel().connectedSegmentsMatches();

  }

}
