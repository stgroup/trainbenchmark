package de.tudresden.inf.st.train.jastadd.operations;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.*;
import de.tudresden.inf.st.train.jastadd.queries.*;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;
import de.tudresden.inf.st.train.jastadd.transformations.inject.*;
import de.tudresden.inf.st.train.jastadd.transformations.repair.*;
import hu.bme.mit.trainbenchmark.benchmark.operations.ModelOperation;
import hu.bme.mit.trainbenchmark.benchmark.operations.ModelOperationFactory;
import hu.bme.mit.trainbenchmark.constants.RailwayOperation;

public class JastaddModelOperationFactory extends ModelOperationFactory<JastaddMatch, JastaddDriver> {

  @Override
  public ModelOperation<? extends JastaddMatch, JastaddDriver> createOperation(final RailwayOperation operationEnum,
                                                                               final String workspaceDir,
                                                                               final JastaddDriver driver) throws Exception {

    switch (operationEnum) {
      // ConnectedSegments
      case CONNECTEDSEGMENTS: {
        final JastaddQuery<JastaddConnectedSegmentsMatch> query = new JastaddQueryConnectedSegments<>(driver);
        final ModelOperation<JastaddConnectedSegmentsMatch, JastaddDriver> operation = ModelOperation.of(query);
        return operation;
      }
      case CONNECTEDSEGMENTS_INJECT: {
        final JastaddQuery<JastaddConnectedSegmentsInjectMatch> query = new JastaddQueryConnectedSegmentsInject<>(driver);
        final JastaddTransformation<JastaddConnectedSegmentsInjectMatch, JastaddDriver> transformation = new JastaddTransformationInjectConnectedSegments<>(
          driver);
        final ModelOperation<JastaddConnectedSegmentsInjectMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }
      case CONNECTEDSEGMENTS_REPAIR: {
        final JastaddQuery<JastaddConnectedSegmentsMatch> query = new JastaddQueryConnectedSegments<>(driver);
        final JastaddTransformation<JastaddConnectedSegmentsMatch, JastaddDriver> transformation = new JastaddTransformationRepairConnectedSegments<>(
          driver);
        final ModelOperation<JastaddConnectedSegmentsMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }

      // PosLength
      case POSLENGTH: {
        final JastaddQuery<JastaddPosLengthMatch> query = new JastaddQueryPosLength<>(driver);
        final ModelOperation<JastaddPosLengthMatch, JastaddDriver> operation = ModelOperation.of(query);
        return operation;
      }
      case POSLENGTH_INJECT: {
        final JastaddQuery<JastaddPosLengthInjectMatch> query = new JastaddQueryPosLengthInject<>(driver);
        final JastaddTransformation<JastaddPosLengthInjectMatch, JastaddDriver> transformation = new JastaddTransformationInjectPosLength<>(
          driver);
        final ModelOperation<JastaddPosLengthInjectMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }
      case POSLENGTH_REPAIR: {
        final JastaddQuery<JastaddPosLengthMatch> query = new JastaddQueryPosLength<>(driver);
        final JastaddTransformation<JastaddPosLengthMatch, JastaddDriver> transformation = new JastaddTransformationRepairPosLength<>(
          driver);
        final ModelOperation<JastaddPosLengthMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }

      // RouteSensor
      case ROUTESENSOR: {
        final JastaddQuery<JastaddRouteSensorMatch> query = new JastaddQueryRouteSensor<>(driver);
        final ModelOperation<JastaddRouteSensorMatch, JastaddDriver> operation = ModelOperation.of(query);
        return operation;
      }
      case ROUTESENSOR_INJECT: {
        final JastaddQuery<JastaddRouteSensorInjectMatch> query = new JastaddQueryRouteSensorInject<>(driver);
        final JastaddTransformation<JastaddRouteSensorInjectMatch, JastaddDriver> transformation = new JastaddTransformationInjectRouteSensor<>(
          driver);
        final ModelOperation<JastaddRouteSensorInjectMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }
      case ROUTESENSOR_REPAIR: {
        final JastaddQuery<JastaddRouteSensorMatch> query = new JastaddQueryRouteSensor<>(driver);
        final JastaddTransformation<JastaddRouteSensorMatch, JastaddDriver> transformation = new JastaddTransformationRepairRouteSensor<>(
          driver);
        final ModelOperation<JastaddRouteSensorMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }

      // SemaphoreNeighbor
      case SEMAPHORENEIGHBOR: {
        final JastaddQuery<JastaddSemaphoreNeighborMatch> query = new JastaddQuerySemaphoreNeighbor<>(driver);
        final ModelOperation<JastaddSemaphoreNeighborMatch, JastaddDriver> operation = ModelOperation.of(query);
        return operation;
      }
      case SEMAPHORENEIGHBOR_INJECT: {
        final JastaddQuery<JastaddSemaphoreNeighborInjectMatch> query = new JastaddQuerySemaphoreNeighborInject<>(driver);
        final JastaddTransformation<JastaddSemaphoreNeighborInjectMatch, JastaddDriver> transformation = new JastaddTransformationInjectSemaphoreNeighbor<>(
          driver);
        final ModelOperation<JastaddSemaphoreNeighborInjectMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }
      case SEMAPHORENEIGHBOR_REPAIR: {
        final JastaddQuery<JastaddSemaphoreNeighborMatch> query = new JastaddQuerySemaphoreNeighbor<>(driver);
        final JastaddTransformation<JastaddSemaphoreNeighborMatch, JastaddDriver> transformation = new JastaddTransformationRepairSemaphoreNeighbor<>(
          driver);
        final ModelOperation<JastaddSemaphoreNeighborMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }

      // SwitchMonitored
      case SWITCHMONITORED: {
        final JastaddQuery<JastaddSwitchMonitoredMatch> query = new JastaddQuerySwitchMonitored<>(driver);
        final ModelOperation<JastaddSwitchMonitoredMatch, JastaddDriver> operation = ModelOperation.of(query);
        return operation;
      }
      case SWITCHMONITORED_INJECT: {
        final JastaddQuery<JastaddSwitchMonitoredInjectMatch> query = new JastaddQuerySwitchMonitoredInject<>(driver);
        final JastaddTransformation<JastaddSwitchMonitoredInjectMatch, JastaddDriver> transformation = new JastaddTransformationInjectSwitchMonitored<>(
          driver);
        final ModelOperation<JastaddSwitchMonitoredInjectMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }
      case SWITCHMONITORED_REPAIR: {
        final JastaddQuery<JastaddSwitchMonitoredMatch> query = new JastaddQuerySwitchMonitored<>(driver);
        final JastaddTransformation<JastaddSwitchMonitoredMatch, JastaddDriver> transformation = new JastaddTransformationRepairSwitchMonitored<>(
          driver);
        final ModelOperation<JastaddSwitchMonitoredMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }

      // SwitchSet
      case SWITCHSET: {
        final JastaddQuery<JastaddSwitchSetMatch> query = new JastaddQuerySwitchSet<>(driver);
        final ModelOperation<JastaddSwitchSetMatch, JastaddDriver> operation = ModelOperation.of(query);
        return operation;
      }
      case SWITCHSET_INJECT: {
        final JastaddQuery<JastaddSwitchSetInjectMatch> query = new JastaddQuerySwitchSetInject<>(driver);
        final JastaddTransformation<JastaddSwitchSetInjectMatch, JastaddDriver> transformation = new JastaddTransformationInjectSwitchSet<>(driver);
        final ModelOperation<JastaddSwitchSetInjectMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }
      case SWITCHSET_REPAIR: {
        final JastaddQuery<JastaddSwitchSetMatch> query = new JastaddQuerySwitchSet<>(driver);
        final JastaddTransformation<JastaddSwitchSetMatch, JastaddDriver> transformation = new JastaddTransformationRepairSwitchSet<>(driver);
        final ModelOperation<JastaddSwitchSetMatch, JastaddDriver> operation = ModelOperation.of(query, transformation);
        return operation;
      }

      default:
        throw new UnsupportedOperationException("Operation " + operationEnum + " not supported.");
    }
  }

}
