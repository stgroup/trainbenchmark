package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSemaphoreNeighborInjectMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.Collection;

public class JastaddQuerySemaphoreNeighborInject<TJastaddDriver extends JastaddDriver>
  extends JastaddQuery<JastaddSemaphoreNeighborInjectMatch> {

  public JastaddQuerySemaphoreNeighborInject(final TJastaddDriver driver) {
    super(RailwayQuery.SEMAPHORENEIGHBOR_INJECT, driver);
  }

  @Override
  public Collection<JastaddSemaphoreNeighborInjectMatch> evaluate() {
    return driver.getModel().semaphoreNeighborInjectMatches();
  }
}
