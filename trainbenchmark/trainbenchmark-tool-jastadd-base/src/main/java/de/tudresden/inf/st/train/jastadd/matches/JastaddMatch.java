package de.tudresden.inf.st.train.jastadd.matches;

import hu.bme.mit.trainbenchmark.benchmark.matches.BaseMatch;

import java.util.Arrays;

public abstract class JastaddMatch extends BaseMatch {

  @Override
  public String toString() {
    return "JastaddMatch [match=" + Arrays.toString(toArray()) + "]";
  }

}
