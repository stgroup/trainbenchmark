package de.tudresden.inf.st.train.jastadd.driver;

import de.tudresden.inf.st.train.jastadd.ast.ASTNode;
import de.tudresden.inf.st.train.jastadd.ast.ASTState;

import java.io.PrintStream;
import java.util.*;

public class CacheCounterReceiver implements ASTState.Trace.Receiver {

  private SortedMap<String, Integer> cacheRead = new TreeMap<>();
  private SortedMap<String, Integer> cacheWrite = new TreeMap<>();
  private SortedMap<String, Integer> flushAttr = new TreeMap<>();

  public void printStats(PrintStream ostream) {
    List<String> attributes = new ArrayList<>();
    attributes.addAll(cacheWrite.keySet());
    Collections.sort(attributes);
    ostream.println("Attribute Name,Cache Write,cacheRead,CacheFlushes,Cache Hit Rate (read/(read+write))");
    for (String attribute: attributes) {
      int write = cacheWrite.getOrDefault(attribute,0);
      int read = cacheRead.getOrDefault(attribute,0);
      int flush = flushAttr.getOrDefault(attribute,0);
      double hitRate = 1.0*read/(read+write);
      ostream.println(attribute + "," + write + "," + read + "," + flush + "," + hitRate);
    }
  }

  @Override
  public void accept(ASTState.Trace.Event event, ASTNode node, String attribute, Object params, Object value) {
    switch (event) {
      case CACHE_READ:
        cacheRead.put(attribute, cacheRead.getOrDefault(attribute,0) + 1);
        break;
      case CACHE_WRITE:
        cacheWrite.put(attribute, cacheWrite.getOrDefault(attribute,0) + 1);
        break;
      case FLUSH_ATTR:
        flushAttr.put(attribute, flushAttr.getOrDefault(attribute,0) + 1);
        break;
    }

  }

  private java.util.Map<String, java.util.Map<String, Integer>> mapHandlers;

  public java.util.Map<String, java.util.Map<String, Integer>> getMapHandlers() {
    return mapHandlers;
  }

  public void setMapHandlers(java.util.Map<String, java.util.Map<String, Integer>> mapHandlers) {
    this.mapHandlers = mapHandlers;
  }
}
