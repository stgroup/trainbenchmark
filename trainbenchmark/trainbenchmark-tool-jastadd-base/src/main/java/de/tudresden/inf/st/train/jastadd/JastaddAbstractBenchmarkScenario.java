package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.comparators.JastaddMatchComparator;
import de.tudresden.inf.st.train.jastadd.driver.CacheCounterReceiver;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriverFactory;
import de.tudresden.inf.st.train.jastadd.matches.JastaddMatch;
import de.tudresden.inf.st.train.jastadd.operations.JastaddModelOperationFactory;
import hu.bme.mit.trainbenchmark.benchmark.phases.BenchmarkScenario;
import hu.bme.mit.trainbenchmark.constants.RailwayOperation;
import hu.bme.mit.trainbenchmark.benchmark.driver.Driver;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;
import de.tudresden.inf.st.train.jastadd.config.JastaddAbstractBenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.result.ResultHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public abstract class JastaddAbstractBenchmarkScenario<TBenchmarkConfigWrapper extends JastaddAbstractBenchmarkConfig> extends
  BenchmarkScenario<JastaddMatch, JastaddDriver, TBenchmarkConfigWrapper> {

  protected final CacheCounterReceiver receiver = new CacheCounterReceiver();

  public JastaddAbstractBenchmarkScenario(final TBenchmarkConfigWrapper bc, boolean flushCaches) throws Exception {
    super(new JastaddDriverFactory(flushCaches), new JastaddModelOperationFactory(), new JastaddMatchComparator(), bc);

    ((JastaddDriverFactory)driverFactory).setReceiver(receiver);
  }

  public void printTraces() {
    try {
      String fileName = ResultHelper.getResultDirectory(this.bc.getProjectName(), bc.getConfigBase().getBenchmarkId()) + "/tracing-" + this.bc.getToolName() + "-" + this.bc.getConfigBase().getWorkload() + "-" + this.bc.getConfigBase().getModelFilename() + "-.csv";
      File file = new File(fileName);

      FileOutputStream fos = new FileOutputStream(file, false);
      PrintStream printStream = new PrintStream(fos);

      receiver.printStats(printStream);
      fos.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
