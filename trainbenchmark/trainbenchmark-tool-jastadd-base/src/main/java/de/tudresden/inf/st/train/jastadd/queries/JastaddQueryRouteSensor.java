package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddRouteSensorMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.Collection;

public class JastaddQueryRouteSensor<TJastaddDriver extends JastaddDriver> extends
  JastaddQuery<JastaddRouteSensorMatch> {

  public JastaddQueryRouteSensor(final TJastaddDriver driver) {
    super(RailwayQuery.ROUTESENSOR, driver);
  }

  @Override
  public Collection<JastaddRouteSensorMatch> evaluate() {
    final Collection<JastaddRouteSensorMatch> matches = driver.getModel().routeSensorMatches();

    return matches;
  }
}
