package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.ast.Switch;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchMonitoredInjectMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.ArrayList;
import java.util.Collection;

public class JastaddQuerySwitchMonitoredInject<TJastaddDriver extends JastaddDriver>
  extends JastaddQuery<JastaddSwitchMonitoredInjectMatch> {

  public JastaddQuerySwitchMonitoredInject(final TJastaddDriver driver) {
    super(RailwayQuery.SWITCHMONITORED_INJECT, driver);
  }

  @Override
  public Collection<JastaddSwitchMonitoredInjectMatch> evaluate() {
    final Collection<JastaddSwitchMonitoredInjectMatch> matches = new ArrayList<>();

    for (final Switch sw : driver.getModel().switchMonitoredInjectMatches()) {
      matches.add(new JastaddSwitchMonitoredInjectMatch(sw));
    }

    return matches;
  }
}
