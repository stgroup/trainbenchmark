package de.tudresden.inf.st.train.jastadd.queries;


import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchSetMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.Collection;


public class JastaddQuerySwitchSet<TJastaddDriver extends JastaddDriver> extends JastaddQuery<JastaddSwitchSetMatch> {

  public JastaddQuerySwitchSet(final TJastaddDriver driver) {
    super(RailwayQuery.SWITCHSET, driver);
  }

  @Override
  public Collection<JastaddSwitchSetMatch> evaluate() {

//		final Collection<Vertex> routes = driver.getVertices(ROUTE);
//		// (route:Route)
//		for (final Vertex route : routes) {
//			// (route:Route)-[:entry]->(semaphore:Semaphore)
//			final Iterable<Vertex> semaphores = JastaddUtil.getAdjacentNodes(route, ENTRY, Direction.OUT, SEMAPHORE);
//
//			for (final Vertex semaphore : semaphores) {
//				// semaphore.signal = "GO"
//				final Object signal = semaphore.property(SIGNAL).value();
//				if (!Signal.GO.toString().equals(signal)) {
//					continue;
//				}
//				// (route:Route)-[:follows]->(swP:SwitchPosition)
//				final Iterable<Vertex> swPs = JastaddUtil.getAdjacentNodes(route, ModelConstants.FOLLOWS, Direction.OUT, SWITCHPOSITION);
//				for (final Vertex swP : swPs) {
//					// (swP:SwitchPosition)-[:target]->(sw:Switch)
//					final Iterable<Vertex> sws = JastaddUtil.getAdjacentNodes(swP, ModelConstants.TARGET, Direction.OUT, SWITCH);
//
//					for (final Vertex sw : sws) {
//						final String currentPosition = (String) sw.property(ModelConstants.CURRENTPOSITION).value();
//						final String position = (String) swP.property(ModelConstants.POSITION).value();
//
//						if (!currentPosition.equals(position)) {
//							matches.add(new JastaddSwitchSetMatch(semaphore, route, swP, sw, position, currentPosition));
//							break;
//						}
//					}
//				}
//			}
//		}

    return driver.getModel().switchSetMatches();
  }
}
