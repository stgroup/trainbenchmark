package de.tudresden.inf.st.train.jastadd.queries;


import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchSetInjectMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.Collection;

public class JastaddQuerySwitchSetInject<TJastaddDriver extends JastaddDriver>
  extends JastaddQuery<JastaddSwitchSetInjectMatch> {

  public JastaddQuerySwitchSetInject(final TJastaddDriver driver) {
    super(RailwayQuery.SWITCHSET_INJECT, driver);
  }

  @Override
  public Collection<JastaddSwitchSetInjectMatch> evaluate() {

//		final Collection<Vertex> switches = driver.getVertices(ModelConstants.SWITCH);
//		for (final Vertex sw : switches) {
//			matches.add(new JastaddSwitchSetInjectMatch(sw));
//		}

    return driver.getModel().switchSetInjectMatches();
  }
}
