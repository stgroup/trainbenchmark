package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Segment;
import hu.bme.mit.trainbenchmark.benchmark.matches.PosLengthMatch;

public class JastaddPosLengthMatch extends JastaddMatch implements PosLengthMatch {

  protected final Segment segment;
  protected final int length;

  public JastaddPosLengthMatch(final Segment segment, final int length) {
    super();
    this.segment = segment;
    this.length = length;
  }

  @Override
  public Segment getSegment() {
    return segment;
  }

  public int getLength() {
    return length;
  }

}
