package de.tudresden.inf.st.train.jastadd.comparators;

import de.tudresden.inf.st.train.jastadd.ast.RailwayElement;
import de.tudresden.inf.st.train.jastadd.matches.JastaddMatch;
import hu.bme.mit.trainbenchmark.benchmark.matches.comparators.BaseMatchComparator;

public class JastaddMatchComparator extends BaseMatchComparator<JastaddMatch, RailwayElement> {

  public JastaddMatchComparator() {
    super(new RailwayElementComparator());
  }

}
