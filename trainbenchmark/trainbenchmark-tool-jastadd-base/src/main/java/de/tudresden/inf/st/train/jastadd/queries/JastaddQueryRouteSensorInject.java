package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddRouteSensorInjectMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.Collection;

public class JastaddQueryRouteSensorInject<TJastaddDriver extends JastaddDriver>
  extends JastaddQuery<JastaddRouteSensorInjectMatch> {

  public JastaddQueryRouteSensorInject(final TJastaddDriver driver) {
    super(RailwayQuery.ROUTESENSOR_INJECT, driver);
  }

  @Override
  public Collection<JastaddRouteSensorInjectMatch> evaluate() {
    final Collection<JastaddRouteSensorInjectMatch> matches = driver.getModel().routeSensorInjectMatches();

    return matches;
  }
}
