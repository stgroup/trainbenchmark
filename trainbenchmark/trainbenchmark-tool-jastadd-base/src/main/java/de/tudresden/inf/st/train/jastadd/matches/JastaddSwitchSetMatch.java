package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.*;
import hu.bme.mit.trainbenchmark.benchmark.matches.SwitchSetMatch;

public class JastaddSwitchSetMatch extends JastaddMatch implements SwitchSetMatch {

  protected final Semaphore semaphore;
  protected final Route route;
  protected final SwitchPosition swP;
  protected final Switch sw;
  protected final Position position;
  protected final Position currentPosition;

  public JastaddSwitchSetMatch(final Semaphore semaphore, final Route route, final SwitchPosition swP, final Switch sw, final Position position,
                               final Position currentPosition) {
    super();
    this.semaphore = semaphore;
    this.route = route;
    this.swP = swP;
    this.sw = sw;
    this.position = position;
    this.currentPosition = currentPosition;
  }

  @Override
  public Semaphore getSemaphore() {
    return semaphore;
  }

  @Override
  public Route getRoute() {
    return route;
  }

  @Override
  public SwitchPosition getSwP() {
    return swP;
  }

  @Override
  public Switch getSw() {
    return sw;
  }

  public Position getPosition() {
    return position;
  }

  public Position getCurrentPosition() {
    return currentPosition;
  }

}
