package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddConnectedSegmentsInjectMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.io.IOException;
import java.util.Collection;

public class JastaddQueryConnectedSegmentsInject<TJastaddDriver extends JastaddDriver>
  extends JastaddQuery<JastaddConnectedSegmentsInjectMatch> {

  public JastaddQueryConnectedSegmentsInject(final TJastaddDriver driver) {
    super(RailwayQuery.CONNECTEDSEGMENTS_INJECT, driver);
  }

  @Override
  public Collection<JastaddConnectedSegmentsInjectMatch> evaluate() throws IOException {

    return driver.getModel().connectedSegmentsInjectMatches();
  }

}
