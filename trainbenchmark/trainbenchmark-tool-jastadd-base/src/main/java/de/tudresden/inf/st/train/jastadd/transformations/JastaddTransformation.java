package de.tudresden.inf.st.train.jastadd.transformations;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddMatch;
import hu.bme.mit.trainbenchmark.benchmark.operations.ModelTransformation;

public abstract class JastaddTransformation<TMatch extends JastaddMatch, TJastaddDriver extends JastaddDriver>
  extends ModelTransformation<TMatch, TJastaddDriver> {

  protected JastaddTransformation(final TJastaddDriver driver) {
    super(driver);
  }

}
