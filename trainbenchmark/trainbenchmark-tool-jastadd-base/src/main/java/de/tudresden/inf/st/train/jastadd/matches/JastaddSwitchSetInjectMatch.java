package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Switch;
import hu.bme.mit.trainbenchmark.benchmark.matches.SwitchSetInjectMatch;

public class JastaddSwitchSetInjectMatch extends JastaddMatch implements SwitchSetInjectMatch {

  protected final Switch sw;

  public JastaddSwitchSetInjectMatch(final Switch sw) {
    super();
    this.sw = sw;
  }

  @Override
  public Switch getSw() {
    return sw;
  }

}
