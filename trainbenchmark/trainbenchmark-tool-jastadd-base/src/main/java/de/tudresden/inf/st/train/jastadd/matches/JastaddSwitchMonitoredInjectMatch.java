package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Switch;
import hu.bme.mit.trainbenchmark.benchmark.matches.SwitchMonitoredInjectMatch;

public class JastaddSwitchMonitoredInjectMatch extends JastaddMatch implements SwitchMonitoredInjectMatch {

  protected final Switch sw;

  public JastaddSwitchMonitoredInjectMatch(final Switch sw) {
    super();
    this.sw = sw;
  }

  @Override
  public Switch getSw() {
    return sw;
  }

}
