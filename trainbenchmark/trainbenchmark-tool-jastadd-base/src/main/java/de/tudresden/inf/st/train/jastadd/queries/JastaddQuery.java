package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddMatch;
import hu.bme.mit.trainbenchmark.benchmark.operations.ModelQuery;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.io.IOException;
import java.util.Collection;

public class JastaddQuery<TPatternMatch extends JastaddMatch> extends
  ModelQuery<TPatternMatch, JastaddDriver> {

  public JastaddQuery(final RailwayQuery query, final JastaddDriver driver) {
    super(query, driver);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Collection<TPatternMatch> evaluate() throws IOException {
    return null;
  }

}
