package de.tudresden.inf.st.train.jastadd.comparators;

import de.tudresden.inf.st.train.jastadd.ast.RailwayElement;

import java.util.Comparator;

public class RailwayElementComparator implements Comparator<RailwayElement> {

  @Override
  public int compare(final RailwayElement r1, final RailwayElement r2) {
    final long id1 = r1.id();
    final long id2 = r2.id();
    return Long.compare(id1, id2);
  }
}
