package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Route;
import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import hu.bme.mit.trainbenchmark.benchmark.matches.RouteSensorInjectMatch;

public class JastaddRouteSensorInjectMatch extends JastaddMatch implements RouteSensorInjectMatch {

  protected final Route route;
  protected final Sensor sensor;

  public JastaddRouteSensorInjectMatch(final Route route, final Sensor sensor) {
    super();
    this.route = route;
    this.sensor = sensor;
  }

  @Override
  public Route getRoute() {
    return route;
  }

  @Override
  public Sensor getSensor() {
    return sensor;
  }

}
