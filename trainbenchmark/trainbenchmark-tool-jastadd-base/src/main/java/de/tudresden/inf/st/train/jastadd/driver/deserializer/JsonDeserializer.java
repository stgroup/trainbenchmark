package de.tudresden.inf.st.train.jastadd.driver.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.tudresden.inf.st.train.jastadd.ast.ASTNode;
import de.tudresden.inf.st.train.jastadd.ast.RailwayContainer;

import java.io.File;
import java.io.IOException;

/**
 * Created by jm on 5/15/17.
 */
public class JsonDeserializer {

  public static RailwayContainer read(File file) {
    ObjectMapper mapper = new ObjectMapper();
    SimpleModule module = new SimpleModule();
    module.addDeserializer(ASTNode.class, new ASTNodeDeserializer());
    mapper.registerModule(module);

    try {
      ASTNode readValue = mapper.readValue(file, ASTNode.class);

      if (readValue instanceof RailwayContainer) {
        return (RailwayContainer) readValue;
      } else {
        throw new RuntimeException("Cound not read a complete model");
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
    throw new RuntimeException("Cound not read the model file " + file.getName());
  }

}
