package de.tudresden.inf.st.train.jastadd.matches;

import de.tudresden.inf.st.train.jastadd.ast.Route;
import de.tudresden.inf.st.train.jastadd.ast.Semaphore;
import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import de.tudresden.inf.st.train.jastadd.ast.TrackElement;
import hu.bme.mit.trainbenchmark.benchmark.matches.SemaphoreNeighborMatch;

public class JastaddSemaphoreNeighborMatch extends JastaddMatch implements SemaphoreNeighborMatch {

  protected final Semaphore semaphore;
  protected final Route route1;
  protected final Route route2;
  protected final Sensor sensor1;
  protected final Sensor sensor2;
  protected final TrackElement te1;
  protected final TrackElement te2;

  public JastaddSemaphoreNeighborMatch(final Semaphore semaphore, final Route route1, final Route route2, final Sensor sensor1, final Sensor sensor2,
                                       final TrackElement te1, final TrackElement te2) {
    super();
    this.semaphore = semaphore;
    this.route1 = route1;
    this.route2 = route2;
    this.sensor1 = sensor1;
    this.sensor2 = sensor2;
    this.te1 = te1;
    this.te2 = te2;
  }

  @Override
  public Semaphore getSemaphore() {
    return semaphore;
  }

  @Override
  public Route getRoute1() {
    return route1;
  }

  @Override
  public Route getRoute2() {
    return route2;
  }

  @Override
  public Sensor getSensor1() {
    return sensor1;
  }

  @Override
  public Sensor getSensor2() {
    return sensor2;
  }

  @Override
  public TrackElement getTe1() {
    return te1;
  }

  @Override
  public TrackElement getTe2() {
    return te2;
  }

}
