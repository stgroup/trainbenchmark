package de.tudresden.inf.st.train.jastadd.queries;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddPosLengthInjectMatch;
import hu.bme.mit.trainbenchmark.constants.RailwayQuery;

import java.util.Collection;

public class JastaddQueryPosLengthInject<TJastaddDriver extends JastaddDriver> extends JastaddQuery<JastaddPosLengthInjectMatch> {

  public JastaddQueryPosLengthInject(final TJastaddDriver driver) {
    super(RailwayQuery.POSLENGTH_INJECT, driver);
  }

  @Override
  public Collection<JastaddPosLengthInjectMatch> evaluate() {

    final Collection<JastaddPosLengthInjectMatch> segments = driver.getModel().posLengthInjectMatches();

    return segments;
  }
}
