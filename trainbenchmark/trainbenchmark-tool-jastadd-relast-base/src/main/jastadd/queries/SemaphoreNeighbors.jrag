aspect Queries {

  syn Collection<JastaddSemaphoreNeighborMatch> RailwayContainer.semaphoreNeighborMatches() {
    java.util.List<JastaddSemaphoreNeighborMatch> matches = new ArrayList<>();

    for (Route route : getRouteList()) {
      matches.addAll(route.semaphoreNeighborMatches());
    }
    return matches;
  }

  syn Collection<JastaddSemaphoreNeighborMatch> Route.semaphoreNeighborMatches() {
    java.util.List<JastaddSemaphoreNeighborMatch> matches = new ArrayList<>();

    // get the Exit Semaphore reference
    Semaphore exitSemaphore = this.exit();

    // get the sensors of the route
    for (Sensor sensor1 : this.requires()) {
      for (TrackElement te1 : sensor1.monitors()) {
        for (TrackElement te2 : te1.connectsTo()) {
          for (Sensor sensor2 : te2.monitoredBy()) {
            for (Route route2 : sensor2.requiredBy()) {
              if (this != route2) {
                if (!route2.hasEntry() || route2.entry() != exitSemaphore) {
                  matches.add(new JastaddSemaphoreNeighborMatch(exitSemaphore, this, route2, sensor1, sensor2, te1, te2));
                }
              }
            }
          }
        }
      }
    }
    return matches;
  }

	syn java.util.Collection<JastaddSemaphoreNeighborInjectMatch> RailwayContainer.semaphoreNeighborInjectMatches() {
    java.util.List<JastaddSemaphoreNeighborInjectMatch> matches = new ArrayList<JastaddSemaphoreNeighborInjectMatch>();
    for (Route route : getRouteList()) {
      if (route.hasEntry()) {
        matches.add(new JastaddSemaphoreNeighborInjectMatch(route, route.entry()));
      }
    }
    return matches;
  }
}
