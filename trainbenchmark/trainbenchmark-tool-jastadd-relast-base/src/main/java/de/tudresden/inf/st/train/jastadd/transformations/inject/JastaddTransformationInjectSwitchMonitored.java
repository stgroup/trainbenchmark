package de.tudresden.inf.st.train.jastadd.transformations.inject;

import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchMonitoredInjectMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.ArrayList;
import java.util.Collection;

public class JastaddTransformationInjectSwitchMonitored<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddSwitchMonitoredInjectMatch, TJastaddDriver> {

  public JastaddTransformationInjectSwitchMonitored(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddSwitchMonitoredInjectMatch> matches) {
    for (final JastaddSwitchMonitoredInjectMatch match : matches) {
      for (Sensor sensor : new java.util.ArrayList<Sensor>(match.getSw().monitoredBy())) {
        match.getSw().removeFromMonitoredBy(sensor);
      }
    }
    driver.flushCache();
  }

}
