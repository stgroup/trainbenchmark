package de.tudresden.inf.st.train.jastadd.transformations.repair;

import de.tudresden.inf.st.train.jastadd.ast.Sensor;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSwitchMonitoredMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationRepairSwitchMonitored<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddSwitchMonitoredMatch, TJastaddDriver> {

  public JastaddTransformationRepairSwitchMonitored(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddSwitchMonitoredMatch> matches) {
    for (final JastaddSwitchMonitoredMatch ssnm : matches) {

      Sensor sensor = new Sensor();
      sensor.setId(driver.nextId());
      sensor.addToMonitors(ssnm.getSw());

      ssnm.getSw().containingRegion().getSensorList().add(sensor);

      ssnm.getSw().addToMonitoredBy(sensor);
    }
    driver.flushCache();
  }

}
