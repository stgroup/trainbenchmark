package de.tudresden.inf.st.train.jastadd.transformations.repair;


import de.tudresden.inf.st.train.jastadd.ast.*;
import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddConnectedSegmentsMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationRepairConnectedSegments<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddConnectedSegmentsMatch, TJastaddDriver> {

  public JastaddTransformationRepairConnectedSegments(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddConnectedSegmentsMatch> matches) {
    for (final JastaddConnectedSegmentsMatch match : matches) {

      final Segment segment2 = match.getSegment2();

      java.util.List<Sensor> sensorsToRemoveSegmentFrom = new java.util.ArrayList<>();

      // delete all references to segment2 (monitoring sensors and other segments connecting to it)
      for (Sensor sensor : new java.util.ArrayList<Sensor>(segment2.monitoredBy())) {
        segment2.removeFromMonitoredBy(sensor);
      }


      for (Region region : driver.getModel().getRegionList()) {
        for (TrackElement element : region.getTrackElementList()) {
          element.removeFromConnectsTo(segment2);
        }
      }

      segment2.removeSelf();

      // connect segment1 to segment3
      match.getSegment1().addToConnectsTo(match.getSegment3());
    }
    driver.flushCache();
  }

}
