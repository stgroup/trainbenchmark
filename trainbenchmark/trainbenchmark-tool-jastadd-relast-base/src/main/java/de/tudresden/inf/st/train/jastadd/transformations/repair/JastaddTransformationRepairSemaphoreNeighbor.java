package de.tudresden.inf.st.train.jastadd.transformations.repair;

import de.tudresden.inf.st.train.jastadd.driver.JastaddDriver;
import de.tudresden.inf.st.train.jastadd.matches.JastaddSemaphoreNeighborMatch;
import de.tudresden.inf.st.train.jastadd.transformations.JastaddTransformation;

import java.util.Collection;

public class JastaddTransformationRepairSemaphoreNeighbor<TJastaddDriver extends JastaddDriver>
  extends JastaddTransformation<JastaddSemaphoreNeighborMatch, TJastaddDriver> {

  public JastaddTransformationRepairSemaphoreNeighbor(final TJastaddDriver driver) {
    super(driver);
  }

  @Override
  public void activate(final Collection<JastaddSemaphoreNeighborMatch> matches) {
    for (final JastaddSemaphoreNeighborMatch match : matches) {
      match.getRoute2().setEntry(match.getSemaphore());
    }
    driver.flushCache();
  }

}
