package de.tudresden.inf.st.trainbenchmark.generator.jastadd.config;

import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfigBuilder;

public class RelASTGeneratorConfigBuilder
  extends GeneratorConfigBuilder<RelASTGeneratorConfig, RelASTGeneratorConfigBuilder> {

  @Override
  public RelASTGeneratorConfig createConfig() {
    checkNotNulls();
    return new RelASTGeneratorConfig(configBase);
  }
}
