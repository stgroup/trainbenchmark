package de.tudresden.inf.st.trainbenchmark.generator.jastadd.config;


import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.config.GeneratorConfigBase;

public class RelASTGeneratorConfig extends GeneratorConfig {

  RelASTGeneratorConfig(final GeneratorConfigBase configBase) {
    super(configBase);
  }

  @Override
  public String getProjectName() {
    return "jastadd-relast";
  }
}
