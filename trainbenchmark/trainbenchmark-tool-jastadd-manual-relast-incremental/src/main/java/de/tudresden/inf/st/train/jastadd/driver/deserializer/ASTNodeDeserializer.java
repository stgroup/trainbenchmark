package de.tudresden.inf.st.train.jastadd.driver.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import de.tudresden.inf.st.train.jastadd.ast.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jm on 5/15/17.
 */
public class ASTNodeDeserializer extends StdDeserializer<ASTNode> {

  public static class IM<Node extends ASTNode> {

    private final Node node;
    private final int ref;

    public IM(Node node, int ref) {
      this.node = node;
      this.ref = ref;
    }

    public Node getNode() {
      return node;
    }
    public int getRef() {
      return ref;
    }

    @Override
    public int hashCode() {
      return node.hashCode() ^ Integer.hashCode(ref);
    }

    @Override
    public boolean equals(Object o) {
      if (!(o instanceof IM)) return false;
      IM pairo = (IM) o;
      return this.node.equals(pairo.getNode()) &&
        this.ref == pairo.getRef();
    }

  }

  final String packageName = "de.tudresden.inf.st.train.jastadd.ast";

  public ASTNodeDeserializer() {
    this(null);
  }

  public ASTNodeDeserializer(Class<?> vc) {
    super(vc);
  }

  private java.util.List<IM<Route>> listRoute_impl_requires = new ArrayList<>();
  private java.util.List<IM<Route>> listRoute_impl_entry = new ArrayList<>();
  private java.util.List<IM<Route>> listRoute_impl_exit = new ArrayList<>();
  private java.util.List<IM<SwitchPosition>> listSwitchPosition_impl_target = new ArrayList<>();
  private java.util.List<IM<Sensor>> listSensor_impl_monitors = new ArrayList<>();
  private java.util.List<IM<Sensor>> listSensor_impl_requiredBy = new ArrayList<>();
  private java.util.List<IM<TrackElement>> listTrackElement_impl_monitoredBy = new ArrayList<>();
  private java.util.List<IM<TrackElement>> listTrackElement_impl_connectsTo = new ArrayList<>();
  private java.util.List<IM<Switch>> listSwitch_impl_positions = new ArrayList<>();

  private Map<Integer, ASTNode> idMap = new HashMap<>();

  @Override
  public RailwayContainer deserialize(JsonParser jp, DeserializationContext ctxt)
    throws IOException {

    JsonNode node = jp.getCodec().readTree(jp);
    RailwayContainer result = deserializeRailwayContainer(node);

    for (IM<Route> e : listRoute_impl_requires) {
      if (idMap.get(e.getRef()) == null) {
        throw new DeserializationException();
      }
      if (e.getNode().get_impl_requires() == null) {
        e.getNode().set_impl_requires(new RefList<>());
      }
      e.getNode().get_impl_requires().add((Sensor)idMap.get(e.getRef()));
    }
    for (IM<Route> e : listRoute_impl_entry) {
      if (idMap.get(e.getRef()) == null) {
        throw new DeserializationException();
      }
      e.getNode().set_impl_entry((Semaphore)idMap.get(e.getRef()));
    }
    for (IM<Route> e : listRoute_impl_exit) {
      e.getNode().set_impl_exit((Semaphore)idMap.get(e.getRef()));
    }
    for (IM<SwitchPosition> e : listSwitchPosition_impl_target) {
      if (idMap.get(e.getRef()) == null) {
        throw new DeserializationException();
      }
      e.getNode().set_impl_target((Switch)idMap.get(e.getRef()));
    }
    for (IM<Sensor> e : listSensor_impl_monitors) {
      if (idMap.get(e.getRef()) == null) {
        throw new DeserializationException();
      }
      if (e.getNode().get_impl_monitors() == null) {
        e.getNode().set_impl_monitors(new RefList<>());
      }
      e.getNode().get_impl_monitors().add((TrackElement)idMap.get(e.getRef()));
    }
    for (IM<Sensor> e : listSensor_impl_requiredBy) {
      if (idMap.get(e.getRef()) == null) {
        throw new DeserializationException();
      }
      if (e.getNode().get_impl_requiredBy() == null) {
        e.getNode().set_impl_requiredBy(new RefList<>());
      }
      e.getNode().get_impl_requiredBy().add((Route)idMap.get(e.getRef()));
    }
    for (IM<TrackElement> e : listTrackElement_impl_monitoredBy) {
      if (idMap.get(e.getRef()) == null) {
        throw new DeserializationException();
      }
      if (e.getNode().get_impl_monitoredBy() == null) {
        e.getNode().set_impl_monitoredBy(new RefList<>());
      }
      e.getNode().get_impl_monitoredBy().add((Sensor)idMap.get(e.getRef()));
    }
    for (IM<TrackElement> e : listTrackElement_impl_connectsTo) {
      if (idMap.get(e.getRef()) == null) {
        throw new DeserializationException();
      }
      if (e.getNode().get_impl_connectsTo() == null) {
        e.getNode().set_impl_connectsTo(new RefList<>());
      }
      e.getNode().get_impl_connectsTo().add((TrackElement)idMap.get(e.getRef()));
    }
    for (IM<Switch> e : listSwitch_impl_positions) {
      if (idMap.get(e.getRef()) == null) {
        throw new DeserializationException();
      }
      if (e.getNode().get_impl_positions() == null) {
        e.getNode().set_impl_positions(new RefList<>());
      }
      e.getNode().get_impl_positions().add((SwitchPosition)idMap.get(e.getRef()));
    }

    return result;
  }

  private RailwayContainer deserializeRailwayContainer(JsonNode node) {
    RailwayContainer instance = new RailwayContainer();

    // Route*
    for (JsonNode child : node.get("c").get("Route").get("c")) {
      instance.addRoute(deserializeRoute(child));
    }

    // Region*
    for (JsonNode child : node.get("c").get("Region").get("c")) {
      instance.addRegion(deserializeRegion(child));
    }

    return instance;
  }

  private Region deserializeRegion(JsonNode node) {
    Region instance = new Region();

    // Id
    Integer id = node.get("c").get("Id").get("v").asInt();
    instance.setId(id);
    idMap.put(id, instance);

    // TrackElement*
    for (JsonNode child : node.get("c").get("TrackElement").get("c")) {
      instance.addTrackElement(deserializeTrackElement(child));
    }

    // Sensor*
    for (JsonNode child : node.get("c").get("Sensor").get("c")) {
      instance.addSensor(deserializeSensor(child));
    }

    return instance;
  }

  private Semaphore deserializeSemaphore(JsonNode node) {
    Semaphore instance = new Semaphore();

    // Id
    Integer id = node.get("c").get("Id").get("v").asInt();
    instance.setId(id);
    idMap.put(id, instance);

    // Signal
    switch (node.get("c").get("Signal").get("v").asText()) {
      case "GO":
        instance.setSignal(Signal.GO);
        break;
      case "STOP":
        instance.setSignal(Signal.STOP);
        break;
      case "FAILURE":
        instance.setSignal(Signal.FAILURE);
        break;
      default:
        throw new DeserializationException();
    }

    return instance;
  }

  private Route deserializeRoute(JsonNode node) {
    Route instance = new Route();

    // Id
    Integer id = node.get("c").get("Id").get("v").asInt();
    instance.setId(id);
    idMap.put(id, instance);

    // Active
    instance.setActive(node.get("c").get("Active").get("v").asBoolean());

    // SwitchPosition
    for (JsonNode child : node.get("c").get("SwitchPosition").get("c")) {
      instance.addSwitchPosition(deserializeSwitchPosition(child));
    }

    // _impl_requires
    for (JsonNode child : node.get("c").get("_impl_requires").get("c")) {
      int ref = child.get("v").asInt();
      this.listRoute_impl_requires.add(new IM<Route>(instance, ref));
    }

    // _impl_Entry
    if (!node.get("c").get("_impl_entry").get("v").isNull()) {
      this.listRoute_impl_entry.add(new IM<Route>(instance, node.get("c").get("_impl_entry").get("v").asInt()));
    }

    /// _impl_Exit
    if (!node.get("c").get("_impl_exit").get("v").isNull()) {
      this.listRoute_impl_exit.add(new IM<Route>(instance, node.get("c").get("_impl_exit").get("v").asInt()));
    }
    return instance;
  }

  private SwitchPosition deserializeSwitchPosition(JsonNode node) {
    SwitchPosition instance = new SwitchPosition();

    // Id
    Integer id = node.get("c").get("Id").get("v").asInt();
    instance.setId(id);
    idMap.put(id, instance);

    // Position
    switch (node.get("c").get("Position").get("v").asText()) {
      case "FAILURE":
        instance.setPosition(Position.FAILURE);
        break;
      case "STRAIGHT":
        instance.setPosition(Position.STRAIGHT);
        break;
      case "DIVERGING":
        instance.setPosition(Position.DIVERGING);
        break;
      default:
        throw new DeserializationException();
    }

    // _impl_Target
    this.listSwitchPosition_impl_target.add(new IM<SwitchPosition>(instance, node.get("c").get("_impl_target").get("v").asInt()));


    return instance;
  }

  private Sensor deserializeSensor(JsonNode node) {
    Sensor instance = new Sensor();

    // Id
    Integer id = node.get("c").get("Id").get("v").asInt();
    instance.setId(id);
    idMap.put(id, instance);

    // _impl_Monitors
    for (JsonNode child : node.get("c").get("_impl_monitors").get("c")) {
      int ref = child.get("v").asInt();
      this.listSensor_impl_monitors.add(new IM<Sensor>(instance, ref));
    }

    // _impl_RequiringRoutes
    for (JsonNode child : node.get("c").get("_impl_requiredBy").get("c")) {
      int ref = child.get("v").asInt();
      this.listSensor_impl_requiredBy.add(new IM<Sensor>(instance, ref));
    }

    return instance;
  }

  private TrackElement deserializeTrackElement(JsonNode node) {

    TrackElement instance;

    if (node.get("t").asText().equals("Segment")) {
      instance = deserializeSegment(node);
    } else if (node.get("t").asText().equals("Switch")) {
      instance = deserializeSwitch(node);
    } else {
      throw new DeserializationException();
    }

    // Id
    Integer id = node.get("c").get("Id").get("v").asInt();
    instance.setId(id);
    idMap.put(id, instance);

    // _impl_MonitoredBy
    for (JsonNode child : node.get("c").get("_impl_monitoredBy").get("c")) {
      int ref = child.get("v").asInt();
      this.listTrackElement_impl_monitoredBy.add(new IM<TrackElement>(instance, ref));
    }

    // _impl_ConnectsTo
    for (JsonNode child : node.get("c").get("_impl_connectsTo").get("c")) {
      int ref = child.get("v").asInt();
      this.listTrackElement_impl_connectsTo.add(new IM<TrackElement>(instance, ref));
    }

    return instance;
  }

  private Segment deserializeSegment(JsonNode node) {
    Segment instance = new Segment();

    // Length
    instance.setLength(node.get("c").get("Length").get("v").asInt());

    // Semaphore*
    for (JsonNode child : node.get("c").get("Semaphore").get("c")) {
      instance.addSemaphore(deserializeSemaphore(child));
    }

    return instance;
  }

  private Switch deserializeSwitch(JsonNode node) {
    Switch instance = new Switch();

    // CurrentPosition
    switch (node.get("c").get("CurrentPosition").get("v").asText()) {
      case "FAILURE":
        instance.setCurrentPosition(Position.FAILURE);
        break;
      case "STRAIGHT":
        instance.setCurrentPosition(Position.STRAIGHT);
        break;
      case "DIVERGING":
        instance.setCurrentPosition(Position.DIVERGING);
        break;
      default:
        throw new DeserializationException();
    }

    // _impl_Positions
    for (JsonNode child : node.get("c").get("_impl_positions").get("c")) {
      int ref = child.get("v").asInt();
      this.listSwitch_impl_positions.add(new IM<Switch>(instance, ref));
    }

    return instance;
  }

}
