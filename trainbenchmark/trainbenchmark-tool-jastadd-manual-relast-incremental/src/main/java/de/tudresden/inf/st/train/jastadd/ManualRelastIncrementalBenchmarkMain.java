package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.ManualRelastIncrementalBenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;

public class ManualRelastIncrementalBenchmarkMain {
  public static void main(final String[] args) throws Exception {
    final ManualRelastIncrementalBenchmarkConfig bc = BenchmarkConfig.fromFile(args[0], ManualRelastIncrementalBenchmarkConfig.class);
    final ManualRelastIncrementalBenchmarkScenario scenario = new ManualRelastIncrementalBenchmarkScenario(bc);
    scenario.performBenchmark();
    scenario.printTraces();
  }
}
