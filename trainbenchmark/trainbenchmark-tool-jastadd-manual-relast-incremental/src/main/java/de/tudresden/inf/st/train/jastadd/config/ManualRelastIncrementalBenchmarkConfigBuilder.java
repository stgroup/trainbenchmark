package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBuilder;

public class ManualRelastIncrementalBenchmarkConfigBuilder extends BenchmarkConfigBuilder<ManualRelastIncrementalBenchmarkConfig, ManualRelastIncrementalBenchmarkConfigBuilder> {

  @Override
  public ManualRelastIncrementalBenchmarkConfig createConfig() {
    checkNotNulls();
    return new ManualRelastIncrementalBenchmarkConfig(configBase);
  }

}
