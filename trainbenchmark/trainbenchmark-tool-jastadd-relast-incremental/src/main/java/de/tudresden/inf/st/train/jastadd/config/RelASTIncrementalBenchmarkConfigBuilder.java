package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBuilder;

public class RelASTIncrementalBenchmarkConfigBuilder extends BenchmarkConfigBuilder<RelASTIncrementalBenchmarkConfig, RelASTIncrementalBenchmarkConfigBuilder> {

  @Override
  public RelASTIncrementalBenchmarkConfig createConfig() {
    checkNotNulls();
    return new RelASTIncrementalBenchmarkConfig(configBase);
  }

}
