package de.tudresden.inf.st.train.jastadd.driver.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import de.tudresden.inf.st.train.jastadd.ast.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jm on 5/15/17.
 */
public class ASTNodeDeserializer extends StdDeserializer<ASTNode> {

  public ASTNodeDeserializer() {
    super((JavaType) null);
  }

  protected ASTNodeDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public ASTNode deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    throw new RuntimeException("this should not be called");
  }
}
