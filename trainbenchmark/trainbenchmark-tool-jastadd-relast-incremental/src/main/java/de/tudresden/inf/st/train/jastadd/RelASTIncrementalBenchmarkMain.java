package de.tudresden.inf.st.train.jastadd;

import de.tudresden.inf.st.train.jastadd.config.RelASTIncrementalBenchmarkConfig;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfig;

public class RelASTIncrementalBenchmarkMain {
  public static void main(final String[] args) throws Exception {
    final RelASTIncrementalBenchmarkConfig bc = BenchmarkConfig.fromFile(args[0], RelASTIncrementalBenchmarkConfig.class);
    final RelASTIncrementalBenchmarkScenario scenario = new RelASTIncrementalBenchmarkScenario(bc);
    scenario.performBenchmark();
    scenario.printTraces();
  }
}
