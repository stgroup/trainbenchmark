package de.tudresden.inf.st.train.jastadd.driver;

import de.tudresden.inf.st.train.jastadd.ast.ASTState;
import de.tudresden.inf.st.train.jastadd.ast.DeserializationException;
import de.tudresden.inf.st.train.jastadd.ast.RailwayContainer;
import hu.bme.mit.trainbenchmark.benchmark.driver.Driver;

import java.io.File;
import java.io.IOException;


public class JastaddDriver extends Driver {

  private final boolean flushCaches;
  private final ASTState.Trace.Receiver receiver;
  private RailwayContainer root;
  private int idCounter = 0;

  public JastaddDriver(boolean flushCaches, ASTState.Trace.Receiver receiver) {
    super();
    this.flushCaches = flushCaches;
    this.receiver = receiver;
  }

  public static JastaddDriver create(boolean flushCaches, ASTState.Trace.Receiver receiver) {
    return new JastaddDriver(flushCaches, receiver);
  }

  public int nextId() {
    idCounter--;
    return idCounter;
  }

  public RailwayContainer getModel() {
    return root;
  }

  @Override
  public void read(final String modelPath) throws IOException {

    File modelFile = new File(modelPath);
    try {
      new RailwayContainer().state().enterConstruction();
      root = RailwayContainer.deserialize(modelFile);
      root.state().exitConstruction();
    } catch (DeserializationException e) {
      throw new IOException(e);
    }

    // enable tracing if there is a receiver
    if (receiver != null) {
      getModel().trace().setReceiver(receiver);
    }
  }

  @Override
  public String getPostfix() {
    return "-jastadd-relast.json";
  }

  public void flushCache() {
    if (flushCaches) {
      getModel().flushTreeCache();
    }
  }
}
