package de.tudresden.inf.st.train.jastadd.test;

import de.tudresden.inf.st.train.jastadd.RelASTIncrementalBenchmarkScenario;
import de.tudresden.inf.st.train.jastadd.config.RelASTIncrementalBenchmarkConfig;
import de.tudresden.inf.st.train.jastadd.config.RelASTIncrementalBenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;
import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkResult;
import hu.bme.mit.trainbenchmark.benchmark.test.TrainBenchmarkTest;

public class RelASTSerializerIncrementalTest extends TrainBenchmarkTest {

  @Override
  protected BenchmarkResult runTest(final BenchmarkConfigBase bcb) throws Exception {
    final RelASTIncrementalBenchmarkConfig bc = new RelASTIncrementalBenchmarkConfigBuilder().setConfigBase(bcb).createConfig();
    final RelASTIncrementalBenchmarkScenario scenario = new RelASTIncrementalBenchmarkScenario(bc);
    final BenchmarkResult result = scenario.performBenchmark();
    return result;
  }

}
