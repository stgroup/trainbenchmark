package de.tudresden.inf.st.trainbenchmark.generator.jastadd;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import de.tudresden.inf.st.trainbenchmark.generator.jastadd.config.ManualIntrinsicGeneratorConfig;
import hu.bme.mit.trainbenchmark.generator.ModelSerializer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hu.bme.mit.trainbenchmark.constants.ModelConstants.*;

public class ManualIntrinsicSerializer extends ModelSerializer<ManualIntrinsicGeneratorConfig> {

	private ASTNonterminal container;

	private static final Map<String, String> STRUCTURE_EDGE_MAP = new HashMap<String, String>()
	{{
		put(ELEMENTS, Children.TRACKELEMENT);
		put(SENSORS, Children.SENSOR);
		put(SEMAPHORES, Children.SEMAPHORE);
		put(FOLLOWS, Children.SWITCHPOSITION);
	}};


	public ManualIntrinsicSerializer(final ManualIntrinsicGeneratorConfig generatorConfig) {
		super(generatorConfig);
	}

	@Override
	public String syntax() {
		return "JASTADD-MANUAL-INTRINSIC";
	}

	@Override
	public void initModel() throws IOException {
		container = new ASTNonterminal(Grammar.RAILWAYCONTAINER);
		container.getChildren().put(Grammar.ROUTE, new ASTList());
		container.getChildren().put(Grammar.REGION, new ASTList());
	}

	@Override
	public void persistModel() throws IOException {

		// Setup mapper with serializers
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(SerializationFeature.INDENT_OUTPUT);
//    mapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
		SimpleModule module = new SimpleModule();
		module.addSerializer(ASTNonterminal.class, new ASTNonterminalSerializer());
		module.addSerializer(ASTList.class, new ASTListSerializer());
		module.addSerializer(ASTOpt.class, new ASTOptSerializer());
		module.addSerializer(ASTTerminal.class, new ASTTerminalSerializer());
		module.addSerializer(ASTEnum.class, new ASTEnumSerializer());
		module.addSerializer(ASTIntrinsicRef.class, new ASTIntrinsicRefSerializer());
		module.addSerializer(IntrinsicRefList.class, new IntrinsicRefListSerializer());
		mapper.registerModule(module);

		// write out JSON
		String jsonPath = gc.getConfigBase().getModelPathWithoutExtension() + "-jastadd-manual-intrinsic.json";
		System.out.println(jsonPath);
		mapper.writeValue(new File(jsonPath), container);
	}

	@Override
	public Object createVertex(
		final int id,
		final String type,
		final Map<String, ? extends Object> attributes,
		final Map<String, Object> outgoingEdges,
		final Map<String, Object> incomingEdges) throws IOException {

		ASTNonterminal n = new ASTNonterminal(id, type);
		switch (type) {
			case REGION:
				// TrackElement*
				n.createListChild(Children.TRACKELEMENT);

				// Sensor*
				n.createListChild(Children.SENSOR);

				// add to parent
				ASTList regionList = (ASTList) container.getChildren().get(Grammar.REGION);
				regionList.addChild(n);
				break;
			case SEMAPHORE:
				// Signal
				if (attributes.get(SIGNAL) != null) {
					n.addChild(Children.SIGNAL, new ASTEnum(Values.ENUM_SIGNAL, attributes.get(SIGNAL)));
				}
				break;
			case ROUTE:
				// Active
				if (attributes.get(ACTIVE) != null) {
					n.addChild(Children.ACTIVE, new ASTTerminal("boolean", attributes.get(ACTIVE)));
				}

				// SwitchPosition
				n.createListChild(Children.SWITCHPOSITION);

				// _impl_RequiredSensors
				n.createIntrinsicRefListChild(Children.REQUIREDSENSOR);

				// Entry
				if (outgoingEdges.get(ENTRY) != null) {
					n.addIntrinsicRefChild(Children.ENTRY, new ASTIntrinsicRef(Grammar.SEMAPHORE, (ASTNonterminal) outgoingEdges.get(ENTRY)));
				} else {
					n.addIntrinsicRefChild(Children.ENTRY, new ASTIntrinsicRef(Grammar.SEMAPHORE, null));
				}

				// Exit
				if (outgoingEdges.get(EXIT) != null) {
					n.addIntrinsicRefChild(Children.EXIT, new ASTIntrinsicRef(Grammar.SEMAPHORE, (ASTNonterminal) outgoingEdges.get(EXIT)));
				} else {
					n.addIntrinsicRefChild(Children.EXIT, new ASTIntrinsicRef(Grammar.SEMAPHORE, null));
				}

				// add to parent
				ASTList routeList = (ASTList) container.getChildren().get(Grammar.ROUTE);
				routeList.addChild(n);
				break;
			case SWITCHPOSITION:

				// Position
				if (attributes.get(POSITION) != null) {
					n.addChild(Children.POSITION, new ASTEnum(Values.ENUM_POSITION, attributes.get(POSITION)));
				}

				// _impl_Target
				if (outgoingEdges.containsKey(TARGET)) {
					n.addChild(Children.TARGET, new ASTIntrinsicRef(Grammar.SWITCH,  (ASTNonterminal)outgoingEdges.get(TARGET)));
				}

				break;
			case SENSOR:
				// _impl_Monitors
				n.createIntrinsicRefListChild(Children.MONITOREDELEMENT);

				// _impl_RequiringRoutes
				n.createIntrinsicRefListChild(Children.REQUIRINGROUTE);

				break;
			case SEGMENT:

				// _impl_MonitoredBy
				n.createIntrinsicRefListChild(Children.MONITOREDBY);

				// _impl_ConnectsTo
				n.createIntrinsicRefListChild(Children.CONNECTSTO);

				// Length
				if (attributes.get(LENGTH) != null) {
					n.addChild(Children.LENGTH, new ASTTerminal("int", attributes.get(LENGTH)));
				}

				// Semaphore*
				n.createListChild(Children.SEMAPHORE);

				break;
			case SWITCH:

				// _impl_MonitoredBy
				n.createIntrinsicRefListChild(Children.MONITOREDBY);

				// _impl_ConnectsTo
				n.createIntrinsicRefListChild(Children.CONNECTSTO);

				// CurrentPosition
				if (attributes.get(POSITION) != null) {
					n.addChild(Children.POSITION, new ASTEnum(Values.ENUM_POSITION, attributes.get(POSITION)));
				}

				// _impl_Positions
				n.createIntrinsicRefListChild(Children.POSITIONS);
				break;
			default:
				System.err.println(String.format("Unknown type of vertex: %s", type));
		}
		return n;
	}

	@Override
	public void createEdge(final String label, final Object from, final Object to) throws IOException {
		ASTNonterminal to_nt = (ASTNonterminal) to;
		ASTNonterminal from_nt = (ASTNonterminal) from;

		if (label.equals(MONITORED_BY)) {

			// direction 1
			IntrinsicRefList listNode = (IntrinsicRefList) to_nt.getChildren().get(Children.MONITOREDELEMENT);
			listNode.addChild(new ASTIntrinsicRef(Grammar.TRACKELEMENT, from_nt));

			// direction 2
			listNode = (IntrinsicRefList) from_nt.getChildren().get(Children.MONITOREDBY);
			listNode.addChild(new ASTIntrinsicRef(Grammar.SENSOR, to_nt));

		} else if (label.equals(REQUIRES)) {
			// route requires sensor
			// direction 1
			IntrinsicRefList listNode = (IntrinsicRefList) from_nt.getChildren().get(Children.REQUIREDSENSOR);
			listNode.addChild(new ASTIntrinsicRef(Grammar.SENSOR, to_nt));

			// direction 2
			listNode = (IntrinsicRefList) to_nt.getChildren().get(Children.REQUIRINGROUTE);
			listNode.addChild(new ASTIntrinsicRef(Grammar.ROUTE,from_nt));

		} else if (label.equals(CONNECTS_TO)) {
			// trackelement connectsto trackelement
			IntrinsicRefList listNode = (IntrinsicRefList) from_nt.getChildren().get(Children.CONNECTSTO);
			listNode.addChild(new ASTIntrinsicRef(Grammar.TRACKELEMENT, to_nt));
 		} else if (STRUCTURE_EDGE_MAP.containsKey(label)) {
			String modelEdgeLabel = STRUCTURE_EDGE_MAP.get(label);
			ASTList listNode = (ASTList) from_nt.getChildren().get(modelEdgeLabel);
			listNode.addChild(to_nt);
		} else {
			throw new RuntimeException("An edge could not be created because '" + label + "' is an umknown label.");

		}
	}

	@Override
	public void setAttribute(final String type, final Object node, final String key, final Object value)
		throws IOException {
//    writer.write(String.format("Attribute. type: %s, node: %s, key: %s, value: %s\n", type, node, key, value));
		// this is always Switch.currentPosition = $value
		if (!key.equals(CURRENTPOSITION)) {
			System.err.println(String.format("SetAttribute. Unexpected key: %s", type));
			return;
		}
		ASTNonterminal n = (ASTNonterminal) node;
		n.getChildren().put(Children.CURRENTPOSITION, new ASTEnum(Values.ENUM_POSITION, value));
	}

	static class Grammar {
		static final String RAILWAYCONTAINER = "RailwayContainer";
		static final String RAILWAYELEMENT = "RailwayElement";
		static final String REGION = "Region";
		static final String SEMAPHORE = "Semaphore";
		static final String ROUTE = "Route";
		static final String SWITCHPOSITION = "SwitchPosition";
		static final String SENSOR = "Sensor";
		static final String TRACKELEMENT = "TrackElement";
		static final String SEGMENT = "Segment";
		static final String SWITCH = "Switch";
		static final String NAME = "Name";
	}

	static class Children {
		static final String ROUTE = "Route";
		static final String REGION = "Region";
		static final String ID = "Id";
		static final String TRACKELEMENT = "TrackElement";
		static final String SENSOR = "Sensor";
		static final String SIGNAL = "Signal";
		static final String ACTIVE = "Active";
		static final String SWITCHPOSITION = "SwitchPosition";
		static final String REQUIREDSENSOR = "_impl_requires";
		static final String REQUIRINGROUTE = "_impl_requiredBy";
		static final String ENTRY = "_impl_entry";
		static final String EXIT = "_impl_exit";
		static final String POSITION = "Position";
		static final String POSITIONS = "_impl_positions";
		static final String TARGET = "_impl_target";
		static final String MONITOREDELEMENT = "_impl_monitors";
		static final String CONNECTSTO = "_impl_connectsTo";
		static final String LENGTH = "Length";
		static final String SEMAPHORE = "Semaphore";
		static final String CURRENTPOSITION = "CurrentPosition";
		static final String VALUE = "Value";
		static final String MONITOREDBY = "_impl_monitoredBy";
	}

	static class Values {
		static final String ENUM_SIGNAL = "de.tudresden.inf.st.train.jastadd.ast.Signal";
		static final String ENUM_POSITION = "de.tudresden.inf.st.train.jastadd.ast.Position";
	}

	private abstract class ASTNode {
		private String kind;
		private String type;

		ASTNode(String kind, String type) {
			this.kind = kind;
			this.type = type;
		}

		public String getKind() {
			return kind;
		}

		public void setKind(String kind) {
			this.kind = kind;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}

	//  @JsonSerialize(using = ASTNonterminalSerializer.class)
	private class ASTNonterminal extends ASTNode {
		//    @JsonSerialize(keyUsing = MapSerializer.class)
		private final Map<String, ASTNode> children;
		private int id;

		ASTNonterminal(String _type) {
			super("NT", _type);
			children = new HashMap<>();
		}

		ASTNonterminal(int id, String _type) {
			super("NT", _type);
			children = new HashMap<>();
			this.setId(id);
		}

		void setId(int id) {
			this.addChild(Children.ID, new ASTTerminal("int", id));
			this.id = id;
		}

		int getId() {
			return this.id;
		}

		Map<String, ASTNode> getChildren() {
			return children;
		}

		ASTNonterminal addChild(String key, ASTNode n) {
			getChildren().put(key, n);
			return this;
		}

		ASTNonterminal createListChild(String context) {
			addChild(context, new ASTList());
			return this;
		}

		ASTNonterminal addOptChild(String context) {
			addChild(context, new ASTOpt());
			return this;
		}

		ASTNonterminal addOptChild(String context, ASTNode n) {
			addChild(context, new ASTOpt(n));
			return this;
		}

		ASTNonterminal addIntrinsicRefChild(String context, ASTIntrinsicRef n) {
			getChildren().put(context, n);
			return this;
		}

		ASTNonterminal createIntrinsicRefListChild(String context) {
			addChild(context, new IntrinsicRefList());
			return this;
		}
	}

	private class ASTList extends ASTNode {
		private final List<ASTNode> children;

		ASTList() {
			super("List", null);
			children = new ArrayList<>();
		}

		List<ASTNode> getChildren() {
			return children;
		}

		ASTList addChild(ASTNode n) {
			getChildren().add(n);
			return this;
		}
	}

	private class ASTOpt extends ASTNode {
		private ASTNode child;

		ASTOpt() {
			super("Opt", null);
			child = null;
		}

		ASTOpt(ASTNode child) {
			super("Opt", null);
			this.child = child;
		}

		boolean hasChild() {
			return child != null;
		}

		ASTNode getChild() {
			return child;
		}

		ASTOpt addChild(ASTNode n) {
			child = n;
			return this;
		}
	}

	private class ASTTerminal extends ASTNode {
		private final Object value;

		ASTTerminal(String _type, Object value) {
			super("t", _type);
			this.value = value;
		}

		public Object getValue() {
			return value;
		}

	}

	private class ASTIntrinsicRef extends ASTNode {
		private Integer value;

		ASTIntrinsicRef(String _type, ASTNonterminal value) {
			super("i", _type);
			if (value != null) {
				this.value = value.getId();
			} else {
				this.value = null;
			}

		}

		public Object getValue() {
			return value;
		}

	}

	private class IntrinsicRefList extends ASTNode {
		private final List<ASTIntrinsicRef> children;

		IntrinsicRefList() {
			super("RefList", null);
			children = new ArrayList<>();
		}

		List<ASTIntrinsicRef> getChildren() {
			return children;
		}

		IntrinsicRefList addChild(ASTIntrinsicRef n) {
			getChildren().add(n);
			return this;
		}
	}

	private class ASTEnum extends ASTNode {
		private final Object value;

		ASTEnum(String _type, Object value) {
			super("enum", _type);
			this.value = value;
		}

		public Object getValue() {
			return value;
		}
	}

	public class ASTNonterminalSerializer extends StdSerializer<ASTNonterminal> {
		public ASTNonterminalSerializer() {
			this(null);
		}

		public ASTNonterminalSerializer(Class<ASTNonterminal> t) {
			super(t);
		}

		@Override
		public void serialize(ASTNonterminal value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			gen.writeStringField("k", value.getKind());
			gen.writeStringField("id", String.valueOf(value.getId()));
			gen.writeStringField("t", value.getType());
			gen.writeObjectField("c", value.getChildren());
			gen.writeEndObject();
		}
	}

	public class ASTListSerializer extends StdSerializer<ASTList> {
		public ASTListSerializer() {
			this(null);
		}

		public ASTListSerializer(Class<ASTList> t) {
			super(t);
		}

		@Override
		public void serialize(ASTList value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			gen.writeStringField("k", value.getKind());
			gen.writeObjectField("c", value.getChildren());
			gen.writeEndObject();
		}
	}

	public class ASTOptSerializer extends StdSerializer<ASTOpt> {
		public ASTOptSerializer() {
			this(null);
		}

		public ASTOptSerializer(Class<ASTOpt> t) {
			super(t);
		}

		@Override
		public void serialize(ASTOpt value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			gen.writeStringField("k", value.getKind());
			if (value.hasChild()) {
				gen.writeObjectField("c", value.getChild());
			}
			gen.writeEndObject();
		}
	}

	public class ASTTerminalSerializer extends StdSerializer<ASTTerminal> {
		public ASTTerminalSerializer() {
			this(null);
		}

		public ASTTerminalSerializer(Class<ASTTerminal> t) {
			super(t);
		}

		@Override
		public void serialize(ASTTerminal value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			gen.writeStringField("k", value.getKind());
			gen.writeObjectField("t", value.getType());
			gen.writeObjectField("v", value.getValue());
			gen.writeEndObject();
		}

	}

	public class ASTEnumSerializer extends StdSerializer<ASTEnum> {
		public ASTEnumSerializer() {
			this(null);
		}

		public ASTEnumSerializer(Class<ASTEnum> t) {
			super(t);
		}

		@Override
		public void serialize(ASTEnum value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			gen.writeStringField("k", value.getKind());
			gen.writeObjectField("t", value.getType());
			gen.writeObjectField("v", value.getValue());
			gen.writeEndObject();
		}

	}


	public class ASTIntrinsicRefSerializer extends StdSerializer<ASTIntrinsicRef> {
		public ASTIntrinsicRefSerializer() {
			this(null);
		}

		public ASTIntrinsicRefSerializer(Class<ASTIntrinsicRef> t) {
			super(t);
		}

		@Override
		public void serialize(ASTIntrinsicRef value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			gen.writeStringField("k", value.getKind());
			gen.writeObjectField("t", value.getType());
			gen.writeObjectField("v", value.getValue());
			gen.writeEndObject();
		}

	}

	public class IntrinsicRefListSerializer extends StdSerializer<IntrinsicRefList> {
		public IntrinsicRefListSerializer() {
			this(null);
		}

		public IntrinsicRefListSerializer(Class<IntrinsicRefList> t) {
			super(t);
		}

		@Override
		public void serialize(IntrinsicRefList value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			gen.writeStringField("k", value.getKind());
			gen.writeObjectField("c", value.getChildren());
			gen.writeEndObject();
		}
	}

}
