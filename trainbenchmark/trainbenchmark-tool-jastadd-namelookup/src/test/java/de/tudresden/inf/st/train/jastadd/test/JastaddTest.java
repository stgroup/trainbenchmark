package de.tudresden.inf.st.train.jastadd.test;

import de.tudresden.inf.st.train.jastadd.NameLookupBenchmarkScenario;
import de.tudresden.inf.st.train.jastadd.config.NameLookupBenchmarkConfig;
import de.tudresden.inf.st.train.jastadd.config.NameLookupBenchmarkConfigBuilder;
import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBase;
import hu.bme.mit.trainbenchmark.benchmark.runcomponents.BenchmarkResult;
import hu.bme.mit.trainbenchmark.benchmark.test.TrainBenchmarkTest;

public class JastaddTest extends TrainBenchmarkTest {

  @Override
  protected BenchmarkResult runTest(final BenchmarkConfigBase bcb) throws Exception {
    final NameLookupBenchmarkConfig bc = new NameLookupBenchmarkConfigBuilder().setConfigBase(bcb).createConfig();
    final NameLookupBenchmarkScenario scenario = new NameLookupBenchmarkScenario(bc);
    final BenchmarkResult result = scenario.performBenchmark();
    return result;
  }

}
