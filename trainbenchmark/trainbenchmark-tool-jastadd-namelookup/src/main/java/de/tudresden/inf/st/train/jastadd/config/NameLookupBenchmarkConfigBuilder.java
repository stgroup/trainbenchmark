package de.tudresden.inf.st.train.jastadd.config;

import hu.bme.mit.trainbenchmark.benchmark.config.BenchmarkConfigBuilder;

public class NameLookupBenchmarkConfigBuilder extends BenchmarkConfigBuilder<NameLookupBenchmarkConfig, NameLookupBenchmarkConfigBuilder> {

  @Override
  public NameLookupBenchmarkConfig createConfig() {
    checkNotNulls();
    return new NameLookupBenchmarkConfig(configBase);
  }

}
